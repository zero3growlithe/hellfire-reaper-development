﻿using UnityEngine;
using UnityEditor;
using System.Reflection;
using System.Collections;
using System.Collections.Generic;

[CanEditMultipleObjects]
[CustomEditor(typeof(LevelWavesController))]
public class ELevelWavesController : Editor {

//*** ATTRIBUTES ***//
	

//*** PROPERTIES ***//
	private LevelWavesController Target {get; set;}

//*** METHODS - PUBLIC ***//
	public override void OnInspectorGUI ()
	{
		if (Target == null)
		{
			Target = (LevelWavesController)target;
		}

		UpdateWavesCollection();
		DrawDefaultInspector();

		if (GUILayout.Button("Add wave") == true)
		{
			AddWave();
		}
	}

//*** METHODS - PROTECTED ***//


//*** METHODS - PRIVATE ***//
	private void AddWave ()
	{
		FieldInfo wavesProperty = Target.GetType().GetField("waves", BindingFlags.Instance | BindingFlags.NonPublic | BindingFlags.GetField | BindingFlags.SetField);
		EnemyWave[] waves = (EnemyWave[])wavesProperty.GetValue(Target);

		List<EnemyWave> newWaves = new List<EnemyWave>(waves);

		newWaves.Add(new EnemyWave());

		waves = newWaves.ToArray();

		waves[waves.Length - 1].SetName("Wave " + (waves.Length - 1));
		wavesProperty.SetValue(Target, waves);

		EditorUtility.SetDirty(Target);
	}

	private void UpdateWavesCollection ()
	{
		FieldInfo wavesProperty = Target.GetType().GetField("waves", BindingFlags.Instance | BindingFlags.NonPublic | BindingFlags.GetField | BindingFlags.SetField);
		EnemyWave[] waves = (EnemyWave[])wavesProperty.GetValue(Target);

		if (waves.Length <= 1)
		{
			return;
		}

		// don't allow the animation times of previous frames to be
		// larger than next frames
		for (int i = 1; i < waves.Length; i++)
		{
			if (waves[i].KeyAnimationTime < waves[i - 1].KeyAnimationTime)
			{
				waves[i].KeyAnimationTime = waves[i - 1].KeyAnimationTime;

				EditorUtility.SetDirty(Target);
			}

			if (waves[i - 1].WaveTimeOut != 0)
			{
				waves[i].KeyAnimationTime = waves[i - 1].KeyAnimationTime + waves[i - 1].WaveTimeOut;
				
				EditorUtility.SetDirty(Target);
			}
		}
	}

}
