﻿using UnityEngine;
using UnityEngine.Serialization;
using System.Collections;

[System.Serializable]
public class PlayerScore {

//*** ATTRIBUTES ***//
	[SerializeField]
	public string name;
	[SerializeField]
	public int points;

//*** PROPERTIES ***//
	public string Name {
		get {return name;}
		private set {name = value;}
	}
	public int Points {
		get {return points;}
		private set {points = value;}
	}

//*** METHODS - PUBLIC ***//
	public PlayerScore (string playerName, int playerScore)
	{
		Name = playerName;
		Points = playerScore;
	}

//*** METHODS - PROTECTED ***//
	

//*** METHODS - PRIVATE ***//

}
