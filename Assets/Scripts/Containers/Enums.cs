﻿using UnityEngine;
using System.Collections;

public static class Enums {

	public enum LevelName {
		UNKNOWN,
		LEVEL_0,
		LEVEL_1,
		LEVEL_2,
		LEVEL_3,
		LEVEL_4
	}

	public enum GameState {
		MAIN_MENU,
		GAME,
		GAME_OVER,
		ANIMATION,
		PAUSE
	}

	public enum MoveDirection {
		NONE,
		UP,
		DOWN,
		LEFT,
		RIGHT
	}

}
