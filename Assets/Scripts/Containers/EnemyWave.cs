﻿using UnityEngine;
using System.Collections;

[System.Serializable]
public class EnemyWave {

//*** ATTRIBUTES ***//
	[Header("[ References ]")]
	[SerializeField]
	private string name = "unknown";
	[SerializeField]
	private GameObject reference;

	[Header("[ Settings ]")]
	[SerializeField]
	private float keyAnimationTime = 0;
	[SerializeField]
	private float waveTimeOut = 30;

	[Space(10)]
	[SerializeField]
	private bool skipWhenDestroyed = false;

//*** PROPERTIES ***//
	// REFERENCES
	public string Name {
		get {return name;}
		private set {name = value;}
	}
	public GameObject Reference {
		get {return reference;}
	}

	// SETTINGS
	public float KeyAnimationTime {
		get {return keyAnimationTime;}
		set {keyAnimationTime = value;}
	}
	public float WaveTimeOut {
		get {return waveTimeOut;}
	}

	public bool SkipWhenDestroyed {
		get {return skipWhenDestroyed;}
	}

	// VARIABLES
	public bool IsDestroyed {get; private set;}
	public float StartTime {get; private set;}

//*** METHODS - PUBLIC ***//
	public void StartWave ()
	{
		if (Reference != null)
		{
			Reference.SetActive(true);
		}
	}

	public void SetIsDestroyedFlag ()
	{
		IsDestroyed = true;
	}

	public void SetName (string text)
	{
		Name = text;
	}
}