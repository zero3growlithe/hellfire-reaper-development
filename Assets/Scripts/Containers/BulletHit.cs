﻿using UnityEngine;
using System.Collections;

public class BulletHit {

//*** ATTRIBUTES ***//
	

//*** PROPERTIES ***//
	public Bullet Reference {get; private set;}
	public GameObject HitObject {get; private set;}
	public int Damage {
		get {return (Reference != null) ? Reference.Power : 0;}
	}

//*** METHODS - PUBLIC ***//
	public BulletHit (Bullet bullet, GameObject hit)
	{
		Reference = bullet;
		HitObject = hit;
	}

//*** METHODS - PROTECTED ***//


//*** METHODS - PRIVATE ***//

}
