﻿using UnityEngine;
using System.Collections;
using System.Collections.Generic;

[System.Serializable]
public class GameSave {

//*** ATTRIBUTES ***//
	[Header("[ Settings ]")]
	[SerializeField]
	private bool isMusicEnabled = true;
	[SerializeField]
	private bool isSoundEnabled = true;

	[Space(10)]
	[SerializeField]
	private float musicVolume = 1.0f;
	[SerializeField]
	private float soundVolume = 1.0f;

//*** PROPERTIES ***//
	// SETTINGS
	public bool IsMusicEnabled {
		get {return isMusicEnabled;}
		private set {isMusicEnabled = value;}
	}
	public bool IsSoundEnabled {
		get {return isSoundEnabled;}
		private set {isSoundEnabled = value;}
	}

	public float MusicVolume {
		get {return musicVolume;}
		private set {musicVolume = value;}
	}
	public float SoundVolume {
		get {return soundVolume;}
		private set {soundVolume = value;}
	}

//*** METHODS - PUBLIC ***//
	// TOGGLES
	public void SetMusicState (bool state)
	{
		IsMusicEnabled = state;
	}

	public void SetSoundState (bool state)
	{
		IsSoundEnabled = state;
	}

	// VOLUME CONTROL
	public void SetMusicVolume (float value)
	{
		MusicVolume = Mathf.Clamp01(value);
	}

	public void SetSoundVolume (float value)
	{
		SoundVolume = Mathf.Clamp01(value);
	}

//*** METHODS - PROTECTED ***//
	

//*** METHODS - PRIVATE ***//


//*** ENUMS ***//

}
