﻿using UnityEngine;
using System.Collections;

public class RotateInFrames : MonoBehaviour {

//*** ATTRIBUTES ***//
	[SerializeField]
	private int frameCount = 3;
	[SerializeField]
	private int frameRate = 12;
	[SerializeField]
	private int direction = 1;

//*** PROPERTIES ***//
	private int FrameCount {
		get {return frameCount;}
	}
	private int FrameRate {
		get {return frameRate;}
	}
	private int Direction {
		get {return direction;}
	}

	private float NextFrameTime {get; set;}
	private int CurrentFrame {get; set;}

//*** METHODS - PUBLIC ***//


//*** METHODS - PROTECTED ***//
	protected virtual void Awake ()
	{
		
	}

	protected virtual void Start ()
	{
		
	}
	
	protected virtual void Update ()
	{
		HandleAnimation();
	}

//*** METHODS - PRIVATE ***//
	private void HandleAnimation ()
	{
		if (Time.time < NextFrameTime)
		{
			return;
		}

		NextFrameTime = Time.time + 1.0f / FrameRate;
		CurrentFrame = (CurrentFrame + 1) % FrameCount;

		UpdateRotation();
	}

	private void UpdateRotation ()
	{
		Vector3 rotation = transform.localEulerAngles;

		rotation.z = CurrentFrame * 360.0f / FrameCount * Direction;

		transform.localEulerAngles = rotation;
	}

}
