﻿using UnityEngine;
using System.Collections;

public class MoveObjectBetweenPoints : MonoBehaviour {

//*** ATTRIBUTES ***//
	[Header("[ References ]")]
	[SerializeField]
	private Transform targetToMove;
	[SerializeField]
	private Transform[] anchorPoints;

	[Header("[ Settings ]")]
	[SerializeField]
	private float moveSpeed = 10f;
	[SerializeField]
	private float placeSwitchDelay = 2f;
	[SerializeField]
	private float exitTimeOut = 10f;

	[Space(10)]
	[SerializeField]
	private bool selectRandomSpot = true;

//*** PROPERTIES ***//
	// REFERENCES
	private Transform TargetToMove {
		get {return targetToMove;}
	}
	private Transform[] AnchorPoints {
		get {return anchorPoints;}
	}

	// SETTINGS
	private float MoveSpeed {
		get {return moveSpeed;}
	}
	private float PlaceSwitchDelay {
		get {return placeSwitchDelay;}
	}
	private float ExitTimeOut {
		get {return exitTimeOut;}
	}

	private bool SelectRandomSpot {
		get {return selectRandomSpot;}
	}

	private int LastPointIndex {get; set;}

//*** METHODS - PUBLIC ***//


//*** METHODS - PROTECTED ***//
	protected virtual void Awake ()
	{
		
	}

	protected virtual void Start ()
	{
		StartCoroutine(MoveBetweenPoints());
	}
	
	protected virtual void Update ()
	{
		
	}

//*** METHODS - PRIVATE ***//
	private IEnumerator MoveBetweenPoints ()
	{
		LastPointIndex = GetNextPointIndex();

		float exitTime = Time.time + ExitTimeOut;
		float nextSwitchTime = Time.time + PlaceSwitchDelay;
		Transform currentTarget = AnchorPoints[LastPointIndex];

		while (true)
		{
			MoveTargetTowardsPoint(currentTarget);

			if (Time.time > nextSwitchTime)
			{
				LastPointIndex = GetNextPointIndex();
				currentTarget = AnchorPoints[LastPointIndex];
				nextSwitchTime = Time.time + PlaceSwitchDelay;
			}

			if (Time.time > exitTime)
			{
				StartCoroutine(MoveTowardsExit());

				break;
			}

			yield return null;
		}

		yield break;
	}

	private IEnumerator MoveTowardsExit ()
	{
		int direction = (TargetToMove.position.x > 0) ? 1 : -1;

		while (true)
		{
			TargetToMove.Translate(
				new Vector3(MoveSpeed * Time.deltaTime * direction, 0, 0),
				Space.World);

			yield return null;
		}
	}

	private void MoveTargetTowardsPoint (Transform point)
	{
		Vector3 targetPosition = TargetToMove.position;

		targetPosition = Vector3.MoveTowards(targetPosition, point.position, MoveSpeed * Time.deltaTime);

		TargetToMove.position = targetPosition;
	}

	private int GetNextPointIndex ()
	{
		if (SelectRandomSpot == true)
		{
			return GetNextRandomIndex();
		}
		else
		{
			return (LastPointIndex + 1) % AnchorPoints.Length;
		}
	}

	private int GetNextRandomIndex ()
	{
		if (AnchorPoints.Length <= 1)
		{
			return (LastPointIndex + 1) % AnchorPoints.Length;
		}

		int currentIndex = LastPointIndex;

		while (currentIndex == LastPointIndex)
		{
			currentIndex = Random.Range(0, AnchorPoints.Length);
		}

		return currentIndex;
	}

}
