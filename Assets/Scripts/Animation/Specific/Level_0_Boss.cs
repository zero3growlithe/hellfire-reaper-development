﻿using UnityEngine;
using System.Collections;

// will be remade to inherit from base class "Boss"
public class Level_0_Boss : MonoBehaviour {

//*** ATTRIBUTES ***//
	[Header("[ References ]")]
	[SerializeField]
	private BossAnimation[] animations;

	[Header("[ Animation settings ]")]
	[SerializeField]
	private int animationFrameRate = 5;
	[SerializeField]
	private int frameCount = 3;
	[SerializeField]
	private int currentAnimationIndex = 0;

	[Header("[ Animator settings ]")]
	[SerializeField]
	private bool useAnimatorAnimationIndex = true;
	[SerializeField]
	private float animatorExposedAnimationIndex = 0;

//*** PROPERTIES ***//
	// REFERENCES
	private BossAnimation[] Animations {
		get {return animations;}
	}

	// ANIMATION SETTINGS
	public int CurrentAnimationIndex {
		get {return currentAnimationIndex;}
		private set {currentAnimationIndex = value;}
	}
	private int AnimationFrameRate {
		get {return animationFrameRate;}
	}
	private int FrameCount {
		get {return frameCount;}
	}

	private bool UseAnimatorAnimationIndex {
		get {return useAnimatorAnimationIndex;}
	}
	private float AnimatorExposedAnimationIndex {
		get {return animatorExposedAnimationIndex;}
	}

	// VARIABLES
	private float NextFrameTime {get; set;}
	private int CurrentSpriteAnimationFrame {get; set;}
	private int LastBossAnimationIndex {get; set;}

//*** METHODS - PUBLIC ***//


//*** METHODS - PROTECTED ***//
	protected virtual void Awake ()
	{
		LastBossAnimationIndex = -1;
	}

	protected virtual void Start ()
	{
		
	}
	
	protected virtual void Update ()
	{
		UpdateAnimationIndex();
		HandleAnimation();
		HandleStateChange();
	}

	protected void OnDestroy ()
	{
		GameActions.Instance.NotifyOnGameOver(true);
	}

//*** METHODS - PRIVATE ***//
	private void UpdateAnimationIndex ()
	{
		if (UseAnimatorAnimationIndex == true)
		{
			CurrentAnimationIndex = (int)Mathf.Round(AnimatorExposedAnimationIndex);
		}
	}

	private void HandleStateChange ()
	{
		if (CurrentAnimationIndex == LastBossAnimationIndex)
		{
			return;
		}

		for (int i = 0; i < Animations.Length; i++)
		{
			Animations[i].SetActive(i == CurrentAnimationIndex);
		}

		LastBossAnimationIndex = CurrentAnimationIndex;
	}

	private void HandleAnimation ()
	{
		if (Time.time < NextFrameTime)
		{
			return;
		}

		SetAnimationFrame(CurrentSpriteAnimationFrame + 1);

		NextFrameTime = Time.time + 1.0f / AnimationFrameRate;
	}

	private void SetAnimationFrame (int frame)
	{
		CurrentSpriteAnimationFrame = frame % FrameCount;

		foreach (BossAnimation animation in Animations)
		{
			animation.SetAnimationValue(CurrentSpriteAnimationFrame);
		}
	}

//*** ENUMS ***//
	[System.Serializable]
	public class BossAnimation {
		[SerializeField]
		private BossAnimationType animationName;
		[SerializeField]
		private SpriteAnimationController animationController;
		[SerializeField]
		private SpriteRenderer targetRenderer;

		public BossAnimationType AnimationName {
			get {return animationName;}
		}
		public SpriteAnimationController AnimationController {
			get {return animationController;}
		}
		public SpriteRenderer TargetRenderer {
			get {return targetRenderer;}
		}

		public void SetActive (bool state)
		{
			TargetRenderer.enabled = state;
		}

		public void SetAnimationValue (int value)
		{
			AnimationController.SetCurrentFrameByValue(value);
		}
	}

	public enum BossAnimationType {
		BOTTOM,
		MOVE_TO_LASER_START,
		MOVE_TO_LASER_HALF,
		LASER_FULL,
		ANGLED_FORWARD,
		MOVE_TO_TOP_DOWN,
		TOP_DOWN
	}

}
