﻿using UnityEngine;
using System.Collections;

public class SpriteAnimationController : MonoBehaviour, IHideable {

//*** ATTRIBUTES ***//
	[Header("[ References ]")]
	[SerializeField]
	private SpriteRenderer targetSpriteRenderer;

	[Header("[ Settings ]")]
	[SerializeField]
	private SpriteConfiguration[] animationFrames;
	[SerializeField]
	private int defaultAnimationFrameRate = 12;
	[SerializeField]
	private float speedMultiplier = 1;

	private int currentAnimationFrame = 0;

	private static readonly string SPRITE_NOT_FOUND_DEBUG_KEY =
		"Requested animation frame with value {0} does not exist.";

//*** PROPERTIES ***//
	// REFERENCES
	public SpriteRenderer TargetSpriteRenderer {
		get {return targetSpriteRenderer;}
	}

	// SETTINGS
	private SpriteConfiguration[] AnimationFrames {
		get {return animationFrames;}
	}
	private int DefaultAnimationFrameRate {
		get {return defaultAnimationFrameRate;}
	}
	private float SpeedMultiplier {
		get {return speedMultiplier;}
		set {speedMultiplier = value;}
	}

	// VARIABLES
	public bool IsTransitioning {get; private set;}
	public int CurrentAnimationFrame {
		get {return currentAnimationFrame;}
		set {
			currentAnimationFrame = value;
			CurrentSpriteConfiguration = AnimationFrames[currentAnimationFrame];
		}
	}
	public int CurrentAnimationValue {
		get {return CurrentSpriteConfiguration != null ? CurrentSpriteConfiguration.StateValue : 0;}
	}

	public int TargetAnimationFrame {get; private set;}
	public SpriteConfiguration CurrentSpriteConfiguration {get; private set;}

	private float DefaultFrameDelay {
		get {return 1.0f / DefaultAnimationFrameRate;}
	}

//*** METHODS - PUBLIC ***//
	// IHIDEABLE INTERFACE METHODS
	public void Activate (bool state = true)
	{
		TargetSpriteRenderer.enabled = state;
	}

	public void Deactivate ()
	{
		TargetSpriteRenderer.enabled = false;
	}

	// CONTROLLER CONTROLS
	public void TransitionToFrameWithName (string name)
	{
		int targetIndex = GetFrameIndexByName(name);

		if (targetIndex == -1)
		{
			Debug.Log(string.Format(SPRITE_NOT_FOUND_DEBUG_KEY, name));

			return;
		}

		TargetAnimationFrame = targetIndex;
	}

	public void SetCurrentFrameByName (string name)
	{
		int targetIndex = GetFrameIndexByName(name);

		if (targetIndex == -1)
		{
			Debug.Log(string.Format(SPRITE_NOT_FOUND_DEBUG_KEY, name));

			return;
		}

		CurrentAnimationFrame = targetIndex;
		TargetAnimationFrame = targetIndex;

		CancelInvoke("MoveTowardsTargetFrame");
		UpdateSpriteRenderer();
	}

	public void TransitionToFrameWithValue (int value)
	{
		int targetIndex = GetFrameIndexByValue(value);

		if (targetIndex == -1)
		{
			Debug.Log(string.Format(SPRITE_NOT_FOUND_DEBUG_KEY, value));

			return;
		}

		TargetAnimationFrame = targetIndex;
	}

	public void SetCurrentFrameByValue (int value)
	{
		int targetIndex = GetFrameIndexByValue(value);

		if (targetIndex == -1)
		{
			Debug.Log(string.Format(SPRITE_NOT_FOUND_DEBUG_KEY, value));

			return;
		}

		CurrentAnimationFrame = targetIndex;
		TargetAnimationFrame = targetIndex;

		CancelInvoke("MoveTowardsTargetFrame");
		UpdateSpriteRenderer();
	}

	// VALUES OVERRIDING
	public void OverrideCurrentFrame (int index)
	{
		CurrentAnimationFrame = index;
		TargetAnimationFrame = index;
	}

	public void OverrideSpeedMultipler (float value)
	{
		SpeedMultiplier = value;
	}

//*** METHODS - PROTECTED ***//
	protected virtual void Awake ()
	{
		
	}

	protected virtual void Start ()
	{
		
	}
	
	protected virtual void Update ()
	{
		UpdateAnimationFrame();
	}

	protected void OnEnable ()
	{
		UpdateAnimationFrame();
	}

	protected void OnDisable ()
	{
		// when object is disabled, force the next target frame without delay
		if (IsTransitioning == true)
		{
			IsTransitioning = false;

			CurrentAnimationFrame = GetNextFrameIndex();

			CancelInvoke("MoveTowardsTargetFrame");
			UpdateSpriteRenderer();
		}
	}

//*** METHODS - PRIVATE ***//
	private void UpdateAnimationFrame ()
	{
		if (CurrentAnimationFrame == TargetAnimationFrame || IsTransitioning == true)
		{
			return;
		}

		IsTransitioning = true;

		// check next frame and its delay
		SpriteConfiguration nextFrame = AnimationFrames[GetNextFrameIndex()];
		float delay = nextFrame.UseDefaultTime ? DefaultFrameDelay : nextFrame.StateDelay;

		Invoke("MoveTowardsTargetFrame", delay / SpeedMultiplier);
	}

	private void UpdateSpriteRenderer ()
	{
		IsTransitioning = false;

		TargetSpriteRenderer.sprite = AnimationFrames[CurrentAnimationFrame].StateSprite;
	}

	private void MoveTowardsTargetFrame ()
	{
		CurrentAnimationFrame = GetNextFrameIndex();

		UpdateSpriteRenderer();
	}

	private int GetNextFrameIndex ()
	{
		return (int)Mathf.MoveTowards(CurrentAnimationFrame, TargetAnimationFrame, 1);
	}

	private int GetFrameIndexByName (string name)
	{
		for (int i = 0; i < AnimationFrames.Length; i++)
		{
			if (AnimationFrames[i].StateName == name)
			{
				return i;
			}
		}

		return -1;
	}
	
	private int GetFrameIndexByValue (int value)
	{
		for (int i = 0; i < AnimationFrames.Length; i++)
		{
			if (AnimationFrames[i].StateValue == value)
			{
				return i;
			}
		}

		return -1;
	}

//*** ENUMS ***//
	[System.Serializable]
	public class SpriteConfiguration {
		[Header("[ Settings ]")]
		[SerializeField]
		private string stateName = "default";
		[SerializeField]
		private int stateValue = 0;
		[SerializeField]
		private Sprite stateSprite;

		[Header("[ Time ]")]
		// when transitioning to this frame, apply this delay before the sprite is set
		[SerializeField]
		private float stateDelay = 1;
		[SerializeField]
		private bool useDefaultTime = true;

		// SETTINGS
		public string StateName {
			get {return stateName;}
		}
		public int StateValue {
			get {return stateValue;}
		}
		public Sprite StateSprite {
			get {return stateSprite;}
		}

		// TIME
		public float StateDelay {
			get {return stateDelay;}
		}
		public bool UseDefaultTime {
			get {return useDefaultTime;}
		}
	}

}
