﻿using UnityEngine;
using System.Collections;

public class LevelWavesController : MonoBehaviour {

//*** ATTRIBUTES ***//
	[Header("[ References ]")]
	[SerializeField]
	private EnemyWave[] waves;
	// [SerializeField]
	// private Animator targetAnimation;

	[Header("[ Settings ]")]
	[SerializeField]
	private float currentAnimationTime = 0;
	[SerializeField]
	private int currentWaveIndex = 0;

//*** PROPERTIES ***//
	public static LevelWavesController Instance {get; private set;}

	private EnemyWave[] Waves {
		get {return waves;}
	}
	// private Animator TargetAnimation {
	// 	get {return targetAnimation;}
	// }

	private float CurrentAnimationTime {
		get {return currentAnimationTime;}
		set {currentAnimationTime = value;}
	}
	private int CurrentWaveIndex {
		get {return currentWaveIndex;}
		set {currentWaveIndex = value;}
	}

	private EnemyWave CurrentWave {
		get {return CurrentWaveIndex < Waves.Length ? Waves[CurrentWaveIndex] : null;}
	}

	private bool IsRunning {get; set;}
	private bool IsPaused {get; set;}

//*** METHODS - PUBLIC ***//
	public void SetRunningState (bool state)
	{
		IsRunning = state;
	}

	public void SetPauseState (bool state)
	{
		IsPaused = state;
	}

	public void ActivateCurrentWave ()
	{
		CurrentWave.StartWave();

		GameActions.Instance.NotifyOnActivateNextEnemyWave(CurrentWave);
	}

	public void SetWaveDestroyedFlag ()
	{
		if (CurrentWave != null)
		{
			CurrentWave.SetIsDestroyedFlag();
		}
	}

//*** METHODS - PROTECTED ***//
	protected virtual void Awake ()
	{
		DisableAllWaves();

		Instance = this;
	}

	protected virtual void Start ()
	{

	}
	
	protected virtual void Update ()
	{
		HandleAnimation();
	}

//*** METHODS - PRIVATE ***//
	private void DisableAllWaves ()
	{
		foreach (EnemyWave wave in Waves)
		{
			if (wave.Reference != null)
			{
				wave.Reference.SetActive(false);
			}
		}
	}

	private void HandleAnimation ()
	{
		if (IsRunning == false || IsPaused == true)
		{
			return;
		}

		int nextWaveIndex = GetNextEnemyWaveIndex();
		EnemyWave nextWave = GetNextEnemyWave();

		bool waveTimeOut =
			CurrentAnimationTime >= nextWave.KeyAnimationTime &&
			nextWaveIndex != CurrentWaveIndex;
		bool skipWave =
			CurrentWave.SkipWhenDestroyed == true &&
			CurrentWave.IsDestroyed == true;

		// set next wave index
		if (waveTimeOut == true || skipWave == true)
		{
			CurrentWaveIndex = nextWaveIndex;

			if (skipWave == true)
			{
				CurrentAnimationTime = nextWave.KeyAnimationTime;
			}

			ActivateCurrentWave();
		}

		CurrentAnimationTime += Time.deltaTime;
	}

	private EnemyWave GetNextEnemyWave ()
	{
		return Waves[GetNextEnemyWaveIndex()];
	}

	private int GetNextEnemyWaveIndex ()
	{
		if (CurrentWaveIndex >= Waves.Length - 1)
		{
			return CurrentWaveIndex;
		}
		else
		{
			return CurrentWaveIndex + 1;
		}
	}

//*** ENUMS ***//
	
}
