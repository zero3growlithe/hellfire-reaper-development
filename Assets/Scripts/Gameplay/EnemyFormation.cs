﻿using UnityEngine;
using System.Collections;

public class EnemyFormation : MonoBehaviour {

//*** ATTRIBUTES ***//
	

//*** PROPERTIES ***//
	

//*** METHODS - PUBLIC ***//
	public void StartShooting (bool state = true)
	{
		SetShootingStateOnShips(state);
	}

	public void StopShooting ()
	{
		StartShooting(false);
	}

//*** METHODS - PROTECTED ***//
	protected virtual void Awake ()
	{
		
	}

	protected virtual void Start ()
	{
		
	}
	
	protected virtual void Update ()
	{
		TryToDestroyIfEmpty();
	}

//*** METHODS - PRIVATE ***//
	private void TryToDestroyIfEmpty ()
	{
		if (transform.childCount == 0)
		{
			Destroy(gameObject);
		}
	}

	private void SetShootingStateOnShips (bool state)
	{
		EnemyGun[] allGuns = transform.GetComponentsInChildren<EnemyGun>();

		foreach (EnemyGun gun in allGuns)
		{
			gun.EnableShooting(state);
		}
	}

}
