﻿using UnityEngine;
using System.Collections;
using System.Collections.Generic;

public class ScoreComparer : IComparer<PlayerScore> {

//*** ATTRIBUTES ***//
	

//*** PROPERTIES ***//
	

//*** METHODS - PUBLIC ***//


//*** METHODS - PROTECTED ***//
	public int Compare (PlayerScore x, PlayerScore y){
		if (x == null) {
			if (y == null){
				// If x is null and y is null, they're 
				// equal.  
				return 0;
			} else {
				// If x is null and y is not null, y 
				// is greater.  
				return -1;
			}
		} else {
			// If x is not null... 
			if (y == null){
				// ...and y is null, x is greater. {
				return 1;
			} else {
				// ...and y is not null, compare the values
				return x.Points.CompareTo(
					y.Points);
			}
		}
	}

//*** METHODS - PRIVATE ***//

}
