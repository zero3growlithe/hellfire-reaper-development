﻿using UnityEngine;
using System.Collections;

public class DrawEmptyGameObject : MonoBehaviour {

//*** ATTRIBUTES ***//
	[SerializeField]
	private float sphereRadius = 0.1f;
	[SerializeField]
	private float colorMultiplier = 2f;
	[SerializeField]
	private Color sphereColor = Color.red;

//*** PROPERTIES ***//
	private float SphereRadius {
		get {return sphereRadius;}
	}
	private float ColorMultiplier {
		get {return colorMultiplier;}
	}
	private Color SphereColor {
		get {return sphereColor;}
	}

//*** METHODS - PUBLIC ***//


//*** METHODS - PROTECTED ***//
	protected void OnDrawGizmos ()
	{
		Gizmos.color = SphereColor * ColorMultiplier;

		Gizmos.DrawSphere(transform.position, SphereRadius);
	}

//*** METHODS - PRIVATE ***//

}
