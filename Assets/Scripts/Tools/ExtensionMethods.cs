﻿using UnityEngine;
using System.Collections;

public static class ExtensionMethods {

	public static void MergeTo (this Transform target, Transform mergeTarget)
	{
		target.SetParent(mergeTarget);
		target.localPosition = Vector3.zero;
	}

}
