using UnityEngine;
using System.Collections;
using System.Reflection;
using System.Linq;

public class ClassValue : MonoBehaviour {

//*** ATTRIBUTES ***//
	[Header("References")]
	[SerializeField]
	private Component sceneComponentInstance;

	[Header("Settings")]
	[SerializeField]
	[Tooltip("Property is cached in Start rather than Awake")]
	private bool lateInitialize = true;
	[SerializeField]
	private bool retryOnFail = false;
	[SerializeField]
	[MultilineAttribute(5)]
	private string targetPropertyPath = "Instance";

//*** PROPERTIES ***//
	// REFERENCES
	public static Assembly CurrentAssembly {get; private set;}

	private Component SceneComponentInstance {
		get {return sceneComponentInstance;}
		set {sceneComponentInstance = value;}
	}
	private object TargetClassInstance {get; set;}

	// SETTINGS
	private bool LateInitialize {
		get {return lateInitialize;}
	}
	private bool RetryOnFail {
		get {return retryOnFail;}
	}
	private string TargetPropertyPath {
		get {return targetPropertyPath;}
		set {targetPropertyPath = value;}
	}

	private PropertyInfo CachedProperty {get; set;}
	private bool IsSetupCorrectly {get; set;}

//*** METHODS - PUBLIC ***//
	public object GetRawValue ()
	{
		if (IsSetupCorrectly == false)
		{
			return 0;
		}

		return CachedProperty.GetValue(TargetClassInstance, null);
	}

	public int GetIntValue ()
	{
		if (IsSetupCorrectly == false)
		{
			return 0;
		}

		return (int)CachedProperty.GetValue(TargetClassInstance, null);
	}

	public float GetFloatValue ()
	{
		if (IsSetupCorrectly == false)
		{
			return 0f;
		}

		return (float)CachedProperty.GetValue(TargetClassInstance, null);
	}

	public string GetStringValue ()
	{
		if (IsSetupCorrectly == false)
		{
			return "null";
		}

		return (string)CachedProperty.GetValue(TargetClassInstance, null);
	}

	public bool GetBoolValue ()
	{
		if (IsSetupCorrectly == false)
		{
			return false;
		}

		return (bool)CachedProperty.GetValue(TargetClassInstance, null);
	}

//*** METHODS - PROTECTED ***//
	protected virtual void Awake ()
	{
		CleanUpPathString();

		TargetClassInstance = SceneComponentInstance;

		// cache current executing assembly
		if (CurrentAssembly == null)
		{
			CurrentAssembly = Assembly.GetExecutingAssembly();
		}

		// cache property info if object't reference exists
		if (LateInitialize == false)
		{
			CacheTargetProperty();
		}
	}

	protected virtual void Start ()
	{
		if (LateInitialize == true)
		{
			CacheTargetProperty();
		}
	}
	
	protected virtual void Update ()
	{
		if (RetryOnFail == true && IsSetupCorrectly == false)
		{
			CacheTargetProperty();
		}
	}

	protected void OnEnable ()
	{
		if (IsSetupCorrectly == true)
		{
			IsSetupCorrectly = false;

			CacheTargetProperty();
		}
	}

//*** METHODS - PRIVATE ***//
	// class instance is not required
	private PropertyInfo GetPropertyInfoFromStaticPath (string path)
	{
		string[] variables = path.Split('.');

		if (path == string.Empty || variables.Length <= 2)
		{
			LogMessage("[ClassValue] Incorred path or not enough arguments (path too short, must be at least 3 variables long)", gameObject);
			return null;
		}

		// get instance of class
		TargetClassInstance = GetClassInstanceByPropertyName(variables[0], variables[1]);

		if (TargetClassInstance == null)
		{
			LogMessage("[ClassValue] Object's instance not found", gameObject);
			return null;
		}

		// create path without static instance reference
		string shortPath = TargetPropertyPath;

		shortPath = shortPath.Remove(0, variables[0].Length + 1);
		shortPath = shortPath.Remove(0, variables[1].Length + 1);

		return GetPropertyInfoFromInstancePath(TargetClassInstance, shortPath);
	}

	private PropertyInfo GetPropertyInfoFromInstancePath (object instance, string path)
	{
		string[] variables = path.Split('.');

		if (instance == null || path == string.Empty)
		{
			return null;
		}

		PropertyInfo targetPropertyInfo = null;

		// get target variable from path
		for (int i = 0; i < variables.Length; i++)
		{
			if (i == variables.Length - 1)
			{
				targetPropertyInfo = GetPropertyInfoByName(TargetClassInstance, variables[i]);
			}
			else
			{
				TargetClassInstance = GetPropertyValueByName(TargetClassInstance, variables[i]);

				if (TargetClassInstance == null)
				{
					LogMessage("[ClassValue] One of the references in the path is null. Canceling.", gameObject);
					return null;
				}
			}
		}

		return targetPropertyInfo;
	}

	// requires class instance reference
	private object GetPropertyValueByName (object classInstance, string propertyName)
	{
		return classInstance.GetType().GetProperty(propertyName).GetValue(classInstance, null);
	}

	// requires class instance reference
	private PropertyInfo GetPropertyInfoByName (object classInstance, string propertyName)
	{
		return classInstance.GetType().GetProperty(propertyName);
	}

	// finds object's instance
	private object GetClassInstanceByPropertyName (string className, string propertyName)
	{
		if (className == string.Empty || propertyName == string.Empty)
		{
			return null;
		}

		System.Type classType = CurrentAssembly.GetType(className);

		if (classType == null)
		{
			return null;
		}

		// get target's instance property and its value
		PropertyInfo instanceProperty = classType.GetProperty(propertyName);

		if (instanceProperty != null)
		{
			object instance = instanceProperty.GetValue(null, null);

			return instance;
		}

		return null;
	}

	private void CacheTargetProperty ()
	{
		// get target property if object instance is given or not
		if (SceneComponentInstance != null)
		{
			CachedProperty = GetPropertyInfoFromInstancePath(SceneComponentInstance, TargetPropertyPath);
		}
		else
		{
			CachedProperty = GetPropertyInfoFromStaticPath(TargetPropertyPath);
		}

		if (CachedProperty != null)
		{
			IsSetupCorrectly = true;
		}
		else 
		{
			LogMessage("[ClassValue] Cached property hasn't been setup correctly. (is not accessible?)", gameObject);
		}
	}

	public void CleanUpPathString ()
	{
		string output = TargetPropertyPath;

		output = output.Replace("\r", string.Empty);
		output = output.Replace("\n", string.Empty);
		output = output.Replace("\r\n", string.Empty);
		output = output.Replace(" ", string.Empty);

		TargetPropertyPath = output;
	}

	private void LogMessage (object message, GameObject target = null)
	{
		if (RetryOnFail == false)
		{
			Debug.Log(message, target);
		}
	}
}
