﻿using UnityEngine;
using System.Collections;

public class BossShip : DestructableShip {

//*** ATTRIBUTES ***//
	[Header("[ Animation ]")]
	[SerializeField]
	private Animator shipAnimator;
	[SerializeField]
	private string explosionAnimationTrigger = "Explode";

	[Header("[ Score ]")]
	[SerializeField]
	private int pointsReward = 100;

//*** PROPERTIES ***//
	// ANIMATION
	private Animator ShipAnimator {
		get {return shipAnimator;}
	}
	private string ExplosionAnimationTrigger {
		get {return explosionAnimationTrigger;}
	}

	// SCORE
	public int PointsReward {
		get {return pointsReward;}
	}

	private bool IsDestroyed {get; set;}

//*** METHODS - PUBLIC ***//
	public override void Destroy ()
	{
		if (IsDestroyed == true)
		{
			return;
		}

		IsDestroyed = true;

		ShipAnimator.SetTrigger(ExplosionAnimationTrigger);
		GameActions.Instance.NotifyOnShipDestroyed(this);
	}

//*** METHODS - PROTECTED ***//
	protected override void Awake ()
	{
		base.Awake();

		GameController.Instance.SetCurrentBoss(this);
	}

	protected override void Start ()
	{
		base.Start();
	}
	
	protected override void Update ()
	{
		base.Update();
	}

//*** METHODS - PRIVATE ***//

}
