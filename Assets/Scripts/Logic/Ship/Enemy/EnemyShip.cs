﻿using UnityEngine;
using System.Collections;

public class EnemyShip : DestructableShip {

//*** ATTRIBUTES ***//
	[Header("[ Extended settings ]")]
	[SerializeField]
	private bool canMove = true;
	[SerializeField]
	private float moveSpeed = 10;
	[SerializeField]
	private float screenKillOffset = 1.2f;
	[SerializeField]
	private float lifetime = 30f;

	[Space(10)]
	[SerializeField]
	private bool lookAtPlayer = false;
	[SerializeField]
	private float rotateSpeed = 30;

	[Space(10)]
	[SerializeField]
	private Transform rootOverride;

	[Header("[ Score ]")]
	[SerializeField]
	private int pointsReward = 100;

//*** PROPERTIES ***//
	// EXTENDED SETTINGS
	private bool CanMove {
		get {return canMove;}
		set {canMove = value;}
	}
	private float MoveSpeed {
		get {return moveSpeed;}
		set {moveSpeed = value;}
	}
	private float ScreenKillOffset {
		get {return screenKillOffset;}
	}
	private float Lifetime {
		get {return lifetime;}
	}

	private bool LookAtPlayer {
		get {return lookAtPlayer;}
		set {lookAtPlayer = value;}
	}
	private float RotateSpeed {
		get {return rotateSpeed;}
	}

	private Transform RootOverride {
		get {return rootOverride;}
		set {rootOverride = value;}
	}

	// SCORE
	public int PointsReward {
		get {return pointsReward;}
	}

	// VARIABLES
	private Transform PlayerTransform {
		get {return GameController.Instance.PlayerShipTransform;}
	}
	private float KillTime {get; set;}

//*** METHODS - PUBLIC ***//
	public void MoveForward (bool state = true)
	{
		CanMove = state;
	}

	public void ShootPlayer (bool state = true)
	{
		LookAtPlayer = state;
	}

	public void OverrideSpeed (float speed)
	{
		MoveSpeed = speed;
	}

//*** METHODS - PROTECTED ***//
	protected override void Awake ()
	{
		base.Awake();

		KillTime = Time.time + Lifetime;
		RootOverride = (RootOverride == null) ? transform : RootOverride;
	}

	protected override void Start ()
	{
		base.Start();
	}
	
	protected override void Update ()
	{
		base.Update();
		UpdateShipPosition();
		UpdateShipRotation();
		KillIfTimeOut();
	}

//*** METHODS - PRIVATE ***//
	private void UpdateShipPosition ()
	{
		if (MoveSpeed == 0 || CanMove == false)
		{
			return;
		}

		RootOverride.Translate(0, MoveSpeed * Time.deltaTime, 0);
	}

	private void UpdateShipRotation ()
	{
		if (LookAtPlayer == false || PlayerTransform == null)
		{
			return;
		}

		float rotateDirection = Mathf.Sign(GetAngleToPlayer());

		RootOverride.Rotate(0, 0, RotateSpeed * Time.deltaTime * rotateDirection);
	}

	private float GetAngleToPlayer ()
	{
		Vector3 playerDirection = PlayerTransform.position - RootOverride.position;
		float shootAngle = Vector2.Angle(RootOverride.up, playerDirection);
		float side = Vector2.Dot(-RootOverride.right, playerDirection);

		return shootAngle * Mathf.Sign(side);
	}

	private void KillIfTimeOut ()
	{
		if (Time.time > KillTime && Lifetime > 0)
		{
			Destroy();
		}
	}
}
