using UnityEngine;
using System.Collections;

public class DestructableShip : MonoBehaviour, IDestructable {

//*** ATTRIBUTES ***//
	[SerializeField]
	private ParticleSystem explosionParticles;

	[Space(10)]
	[SerializeField]
	private int lives = 10;
	[SerializeField]
	private bool destroyCompletely = true;

//*** PROPERTIES ***//
	// REFERENCES
	protected ParticleSystem ExplosionParticles {
		get {return explosionParticles;}
	}

	// SETTINGS
	public int Lives {
		get {return lives;}
	}
	protected bool DestroyCompletely {
		get {return destroyCompletely;}
	}

	// VARIABLES
	public int CurrentLifeCount {get; set;}

//*** METHODS - PUBLIC ***//
	public virtual void Destroy ()
	{
		SetExplosionState(true);

		if (CurrentLifeCount == 0)
		{
			GameActions.Instance.NotifyOnShipDestroyed(this);
		}

		if (DestroyCompletely == false)
		{
			gameObject.SetActive(false);
		}
		else
		{
			Destroy(ExplosionParticles.gameObject, ExplosionParticles.main.duration);
			Destroy(gameObject);
		}
	}

	public virtual void Revive ()
	{
		CurrentLifeCount = Lives;
		
		SetExplosionState(false);
		gameObject.SetActive(true);
	}

//*** METHODS - PROTECTED ***//
	protected virtual void Awake ()
	{
		CurrentLifeCount = Lives;

		AttachToEvents();
	}

	protected virtual void Start ()
	{
		
	}
	
	protected virtual void Update ()
	{
		
	}

	protected virtual void HandleOnBulletHitEvent (BulletHit hit)
	{
		Transform hitTransform = hit.HitObject.transform;

		if (hitTransform != transform && hitTransform.IsChildOf(transform) == false)
		{
			return;
		}

		CurrentLifeCount = (int)Mathf.MoveTowards(CurrentLifeCount, 0, hit.Damage);

		CheckIfDestroyed();
	}

	protected virtual void OnCollisionEnter2D (Collision2D hit)
	{

	}

	protected void OnDestroy ()
	{
		DetachFromEvents();
	}

//*** METHODS - PRIVATE ***//
	private void AttachToEvents ()
	{
		GameActions.OnBulletHit += HandleOnBulletHitEvent;
	}

	private void DetachFromEvents ()
	{
		GameActions.OnBulletHit -= HandleOnBulletHitEvent;
	}

	private void SetExplosionState (bool state)
	{
		ExplosionParticles.Stop();
		ExplosionParticles.gameObject.SetActive(state);

		if (state == true)
		{
			ReferenceManager.Instance.StoreTargetInContainer(ExplosionParticles.transform);
		}
		else
		{
			ExplosionParticles.transform.MergeTo(transform);
		}
	}

	private void CheckIfDestroyed ()
	{
		if (CurrentLifeCount == 0)
		{
			Destroy();
		}
	}
}