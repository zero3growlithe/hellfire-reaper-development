﻿using UnityEngine;
using System.Collections;

public class ShipModel : Model {

//*** ATTRIBUTES ***//
	[Header("[ References ]")]
	[SerializeField]
	private DestructableShip shipDestructor;

	[Header("[ Movement ]")]
	[SerializeField]
	private float forwardMoveSpeed = 10;
	[SerializeField]
	private float sideMoveSpeed = 20;
	[SerializeField]
	private float speedMultiplier = 20;

	[Header("[ Weapons ]")]
	[SerializeField]
	private ShipGunsController levelOneGuns;

//*** PROPERTIES ***//
	// REFERENCES
	private DestructableShip ShipDestructor {
		get {return shipDestructor;}
	}

	// MOVEMENTS
	private float ForwardMoveSpeed {
		get {return forwardMoveSpeed;}
	}
	private float SideMoveSpeed {
		get {return sideMoveSpeed;}
	}
	private float SpeedMultiplier {
		get {return speedMultiplier;}
	}

	// WEAPONS
	private ShipGunsController LevelOneGuns {
		get {return levelOneGuns;}
	}

	// VARIABLES
	public int CurrentLifeCount {get; private set;}

	private Rigidbody2D TargetRigidbody {get; set;}

	private Vector2 CurrentMoveSpeed {get; set;}
	private bool IsShooting {get; set;}

//*** METHODS - PUBLIC ***//
	public void ResetLives ()
	{
		CurrentLifeCount = GameSettingsManager.Instance.PlayerLives;
	}

	public void Respawn (bool invincibility = true)
	{
		if (CurrentLifeCount == 0)
		{
			return;
		}

		CurrentLifeCount = (int)Mathf.MoveTowards(CurrentLifeCount, 0, 1);

		RestoreShip(invincibility);
	}

	public void RestoreShip (bool invincibility = true)
	{
		ShipDestructor.Revive();

		if (invincibility == true)
		{
			EnableTemporaryInvurability();
		}
	}

	public void Shoot ()
	{
		IsShooting = true;

		LevelOneGuns.Shoot();
	}

	public void Move (Vector2 speed)
	{
		CurrentMoveSpeed = speed;
	}

	public void Disable ()
	{
		gameObject.SetActive(false);
	}

//*** METHODS - PROTECTED ***//
	protected override void Awake ()
	{
		base.Awake();

		TargetRigidbody = GetComponent<Rigidbody2D>();
	}

	protected override void Start ()
	{
		base.Start();

		GameActions.OnShipDestroyed += HandleOnShipDestroyedEvent;
	}
	
	protected override void Update ()
	{
		base.Update();
		UpdateMovement();
		UpdateMovementAnimation();
		ResetInputStates();
	}

	protected void LateUpdate ()
	{
		
	}

	protected void OnEnable ()
	{
		EnableCollider();
	}

	protected void OnDestroy ()
	{
		GameActions.OnShipDestroyed -= HandleOnShipDestroyedEvent;
	}

//*** METHODS - PRIVATE ***//
	private void HandleOnShipDestroyedEvent (DestructableShip ship)
	{
		if (ship.gameObject == gameObject && CurrentLifeCount == 0)
		{
			GameActions.Instance.NotifyOnGameOver(false);
		}
	}

	private void EnableTemporaryInvurability ()
	{
		AllowShipToColliderWithBullets(false);

		((ShipView)CurrentView).EnableRespawnAnimation();
		
		Invoke("EnableCollider", ((ShipView)CurrentView).RespawnAnimationDuration);	
	}

	private void EnableCollider ()
	{
		AllowShipToColliderWithBullets(true);
	}

	private void AllowShipToColliderWithBullets (bool state)
	{
		Physics2D.IgnoreLayerCollision(
			ReferenceManager.Instance.EnemyShipLayer,
			ReferenceManager.Instance.PlayerShipLayer,
			state == false);
		Physics2D.IgnoreLayerCollision(
			ReferenceManager.Instance.EnemyBulletLayer,
			ReferenceManager.Instance.PlayerShipLayer,
			state == false);
		Physics2D.IgnoreLayerCollision(
			ReferenceManager.Instance.EnemyDestructableBulletLayer,
			ReferenceManager.Instance.PlayerShipLayer,
			state == false);
	}

	private void UpdateMovement ()
	{
		float forwardMovement = CurrentMoveSpeed.y * ForwardMoveSpeed;
		float sideMovement = CurrentMoveSpeed.x * SideMoveSpeed;

		Vector2 finalForce = new Vector2(sideMovement, forwardMovement);

		TargetRigidbody.AddForce(finalForce * SpeedMultiplier * Time.deltaTime);
	}

	private void UpdateMovementAnimation ()
	{
		// if both are on or off then the animation is default, otherwise it's directional
		bool isMovingForward = CurrentMoveSpeed.y > 0;

		if (CurrentMoveSpeed.x != 0)
		{
			if (CurrentMoveSpeed.x < 0)
			{
				((ShipView)CurrentView).SetAnimationState(ShipView.ShipState.MOVE_LEFT, isMovingForward);
			}
			else
			{
				((ShipView)CurrentView).SetAnimationState(ShipView.ShipState.MOVE_RIGHT, isMovingForward);
			}
		}
		else
		{
			((ShipView)CurrentView).SetAnimationState(ShipView.ShipState.DEFAULT, isMovingForward);
		}
	}

	private void ResetInputStates ()
	{
		CurrentMoveSpeed = Vector2.zero;
		IsShooting = false;
	}

//*** ENUMS ***//

}
