﻿using UnityEngine;
using System.Collections;

public class ShipController : Controller, IControllable {

//*** ATTRIBUTES ***//
	

//*** PROPERTIES ***//
	public int CurrentLifeCount {
		get {return (CurrentModel != null) ? ((ShipModel)CurrentModel).CurrentLifeCount : 0;}
	}

//*** METHODS - PUBLIC ***//
	public void ResetLives ()
	{
		((ShipModel)CurrentModel).ResetLives();
	}

	public void Respawn (bool invincibility = true)
	{
		((ShipModel)CurrentModel).Respawn(invincibility);
	}

	public void RestoreShip (bool invincibility = true)
	{
		((ShipModel)CurrentModel).RestoreShip(invincibility);
	}

	public void Shoot ()
	{
		((ShipModel)CurrentModel).Shoot();
	}

	public void Move (Vector2 speed)
	{
		((ShipModel)CurrentModel).Move(speed);
	}

	public void Disable ()
	{
		((ShipModel)CurrentModel).Disable();
	}

	public override void Initialize ()
	{
		base.Initialize();

		ResetLives();
	}

//*** METHODS - PROTECTED ***//
	protected override void Awake ()
	{
		// forced manual initialization
	}

	protected override void Start ()
	{
		base.Start();	
	}
	
	protected override void Update ()
	{
		base.Update();	
	}

//*** METHODS - PRIVATE ***//

}
