﻿using UnityEngine;
using System.Collections;

public class ShipView : View {

//*** ATTRIBUTES ***//
	[Header("[ Animation ]")]
	[SerializeField]
	private SpriteAnimationController normalAnimation;
	[SerializeField]
	private SpriteAnimationController thrustAnimation;

	[Space(10)]
	[SerializeField]
	private string turnLeftAnimationName = "LeftFull";
	[SerializeField]
	private string turnRightAnimationName = "RightFull";
	[SerializeField]
	private string neutralAnimationName = "Default";

	[Space(10)]
	[SerializeField]
	private float respawnAnimationDuration = 3;

//*** PROPERTIES ***//
	private SpriteAnimationController NormalAnimation {
		get {return normalAnimation;}
	}
	private SpriteAnimationController ThrustAnimation {
		get {return thrustAnimation;}
	}

	private string TurnLeftAnimationName {
		get {return turnLeftAnimationName;}
	}
	private string TurnRightAnimationName {
		get {return turnRightAnimationName;}
	}
	private string NeutralAnimationName {
		get {return neutralAnimationName;}
	}

	public float RespawnAnimationDuration {
		get {return respawnAnimationDuration;}
	}

//*** METHODS - PUBLIC ***//
	public void EnableRespawnAnimation ()
	{
		StopCoroutine("RespawnAnimation");
		StartCoroutine("RespawnAnimation");
	}

	public void SetAnimationState (ShipState state, bool isBoosting)
	{
		// set target animation frame to transition to
		string transitionTargetName = NeutralAnimationName;

		switch (state)
		{
			case ShipState.MOVE_LEFT:
				transitionTargetName = TurnLeftAnimationName;
				break;
			case ShipState.MOVE_RIGHT:
				transitionTargetName = TurnRightAnimationName;
				break;
		}

		// enable selected animation depending on boost state
		ThrustAnimation.Activate(isBoosting == true);
		NormalAnimation.Activate(isBoosting == false);

		// update transition target
		NormalAnimation.TransitionToFrameWithName(transitionTargetName);
		ThrustAnimation.TransitionToFrameWithName(transitionTargetName);
	}

//*** METHODS - PROTECTED ***//
	protected override void Awake ()
	{
		base.Awake();	

		NormalAnimation.SetCurrentFrameByName(NeutralAnimationName);
		ThrustAnimation.SetCurrentFrameByName(NeutralAnimationName);
	}

	protected override void Start ()
	{
		base.Start();	
	}
	
	protected override void Update ()
	{
		base.Update();	
	}

	protected void OnEnable ()
	{
		SetShipVisibility(true);
	}

//*** METHODS - PRIVATE ***//
	private IEnumerator RespawnAnimation ()
	{
		float endTime = Time.time + RespawnAnimationDuration;
		bool newState = true;

		while (Time.time < endTime)
		{
			SetShipVisibility(newState);

			newState = !newState;

			yield return null;
		}

		SetShipVisibility(true);

		yield break;
	}

	private void SetShipVisibility (bool state)
	{
		NormalAnimation.TargetSpriteRenderer.enabled = state;
		ThrustAnimation.TargetSpriteRenderer.enabled = state;
	}

//*** ENUMS ***//
	public enum ShipState {
		DEFAULT,
		MOVE_LEFT,
		MOVE_RIGHT
	}

}
