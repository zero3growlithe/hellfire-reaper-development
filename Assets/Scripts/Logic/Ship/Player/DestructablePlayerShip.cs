﻿using UnityEngine;
using System.Collections;

public class DestructablePlayerShip : DestructableShip {

//*** ATTRIBUTES ***//
	

//*** PROPERTIES ***//
	

//*** METHODS - PUBLIC ***//


//*** METHODS - PROTECTED ***//
	protected override void Awake ()
	{
		base.Awake();	
	}

	protected override void Start ()
	{
		base.Start();	
	}
	
	protected override void Update ()
	{
		base.Update();	
	}

	protected override void OnCollisionEnter2D (Collision2D hit)
	{
		if (hit.gameObject.tag == ReferenceManager.Instance.ShipTag)
		{
			CurrentLifeCount = 0;

			Destroy();
		}
	}

//*** METHODS - PRIVATE ***//

}
