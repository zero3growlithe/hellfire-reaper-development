﻿using UnityEngine;
using System.Collections;
using System.Collections.Generic;

public class FormationSpawner : ObjectSpawner {

//*** ATTRIBUTES ***//
	[Header("[ Extended Settings ]")]
	[SerializeField]
	private float fleetSpeed = 1;
	[SerializeField]
	private string fleetName = "EnemyShipsFleet";

//*** PROPERTIES ***//
	// EXTENDED SETTINGS
	private float FleetSpeed {
		get {return fleetSpeed;}
	}
	private string FleetName {
		get {return fleetName;}
	}

//*** METHODS - PUBLIC ***//
	

//*** METHODS - PROTECTED ***//
	protected override void Awake ()
	{
		base.Awake();
	}

	protected override void Start ()
	{
		base.Start();
	}
	
	protected override void Update ()
	{
		base.Update();
	}

	protected override List<EnemyShip> SpawnEntitiesInAllSpots ()
	{
		List<EnemyShip> spawnedEnemies = new List<EnemyShip>();
		Transform formationContainer = new GameObject(FleetName).transform;

		formationContainer.gameObject.AddComponent<EnemyFormation>();

		foreach (SpawnReference toSpawn in ObjectsToSpawn)
		{
			EnemyShip newEnemy = SpawnEntity(toSpawn);

			// set formation parameters
			newEnemy.OverrideSpeed(FleetSpeed);
			newEnemy.transform.SetParent(formationContainer);

			spawnedEnemies.Add(newEnemy);
		}

		return spawnedEnemies;
	}

//*** METHODS - PRIVATE ***//

}
