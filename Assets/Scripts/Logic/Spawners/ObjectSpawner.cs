﻿using UnityEngine;
using System.Collections;
using System.Collections.Generic;

public class ObjectSpawner : MonoBehaviour {

//*** ATTRIBUTES ***//
	[Header("[ References ]")]
	[SerializeField]
	private SpawnReference[] objectsToSpawn;
	[SerializeField]
	private bool spawnOnSelf = true;

	[Header("[ Settings ]")]
	[SerializeField]
	private SpawnMethod spawnType;
	[SerializeField]
	private int spawnCount = 5;
	[SerializeField]
	private float spawnRate = 10;

	[Space(10)]
	[SerializeField]
	private float startDelay = 0;

	[Space(10)]
	[SerializeField]
	private bool useSpawnerScale = false;
	[SerializeField]
	private bool spawnOnAwake = true;

//*** PROPERTIES ***//
	// REFERENCES
	protected SpawnReference[] ObjectsToSpawn {
		get {return objectsToSpawn;}
	}
	protected bool SpawnOnSelf {
		get {return spawnOnSelf;}
	}

	// SETTINGS
	protected SpawnMethod SpawnType {
		get {return spawnType;}
	}
	protected int SpawnCount {
		get {return spawnCount;}
	}
	protected float SpawnRate {
		get {return spawnRate;}
	}

	protected float StartDelay {
		get {return startDelay;}
	}

	protected bool UseSpawnerScale {
		get {return useSpawnerScale;}
	}
	protected bool SpawnOnAwake {
		get {return spawnOnAwake;}
	}

	protected bool IsSpawning {get; set;}
	protected int LastSpawnPointIndex {get; set;}

//*** METHODS - PUBLIC ***//
	public void StartSpawning ()
	{
		if (IsSpawning == true)
		{
			return;
		}

		StopAllCoroutines();
		StartCoroutine(HandleEntitiesSpawning());

		IsSpawning = true;
	}

	public void Restart ()
	{
		IsSpawning = false;

		StopAllCoroutines();
	}

//*** METHODS - PROTECTED ***//
	protected virtual void Awake ()
	{

	}

	protected virtual void Start ()
	{
		if (SpawnOnAwake == true)
		{
			StartSpawning();
		}
	}
	
	protected virtual void Update ()
	{
		
	}

	protected virtual IEnumerator HandleEntitiesSpawning ()
	{
		int spawnedEntities = 0;
		float nextSpawnTime = Time.time + StartDelay;

		while (spawnedEntities < SpawnCount)
		{
			if (Time.time > nextSpawnTime)
			{
				SpawnEntities();

				nextSpawnTime = Time.time + 1.0f / SpawnRate;
				spawnedEntities++;
			}

			yield return null;
		}

		IsSpawning = false;

		yield break;
	}

	protected virtual List<EnemyShip> SpawnEntities ()
	{
		switch (SpawnType)
		{
			case SpawnMethod.ALL_AT_ONCE:
				return SpawnEntitiesInAllSpots();
			case SpawnMethod.RANDOM:
				return SpawnEntityInRandomSpot();
			case SpawnMethod.SEQUENCE:
				return SpawnEntityInNextSpot();
		}

		return new List<EnemyShip>();
	}

	protected virtual List<EnemyShip> SpawnEntitiesInAllSpots ()
	{
		List<EnemyShip> spawnedEnemies = new List<EnemyShip>();

		foreach (SpawnReference toSpawn in ObjectsToSpawn)
		{
			spawnedEnemies.Add(SpawnEntity(toSpawn));
		}

		return spawnedEnemies;
	}

	protected virtual List<EnemyShip> SpawnEntityInRandomSpot ()
	{
		int randomSpot = Random.Range(0, ObjectsToSpawn.Length);
		List<EnemyShip> spawnedEnemies = new List<EnemyShip>();

		spawnedEnemies.Add(SpawnEntity(ObjectsToSpawn[randomSpot]));

		return spawnedEnemies;
	}

	protected virtual List<EnemyShip> SpawnEntityInNextSpot ()
	{
		List<EnemyShip> spawnedEnemies = new List<EnemyShip>();

		spawnedEnemies.Add(SpawnEntity(ObjectsToSpawn[LastSpawnPointIndex]));

		LastSpawnPointIndex = (LastSpawnPointIndex + 1) % ObjectsToSpawn.Length;

		return spawnedEnemies;
	}

	protected virtual EnemyShip SpawnEntity (SpawnReference toSpawn)
	{
		Transform point = (SpawnOnSelf == false && toSpawn.SpawnPoint != null) ? toSpawn.SpawnPoint : transform;
		GameObject spawnedEntity = (GameObject)Instantiate(toSpawn.Reference, point.position, point.rotation);

		SpawnedJunkManager.Instance.AddJunk(spawnedEntity);

		if (UseSpawnerScale == true)
		{
			SetEntityScale(spawnedEntity.transform);
		}

		ReferenceManager.Instance.StoreTargetInContainer(spawnedEntity.transform);

		return spawnedEntity.GetComponent<EnemyShip>();
	}

//*** METHODS - PRIVATE ***//
	private void SetEntityScale (Transform target)
	{
		// using this because Vector3.Scale doesn't seem to work as expected
		Vector3 currentScale = target.localScale;
		Vector3 targetScale = transform.localScale;

		currentScale = new Vector3(
			currentScale.x * targetScale.x,
			currentScale.y * targetScale.y,
			currentScale.z * targetScale.z);

		target.localScale = currentScale;
	}

//*** ENUMS ***//
	[System.Serializable]
	public class SpawnReference {
		[SerializeField]
		private string name;
		[SerializeField]
		private GameObject reference;
		[SerializeField]
		private Transform spawnPoint;

		public string Name {
			get {return name;}
		}
		public GameObject Reference {
			get {return reference;}
		}
		public Transform SpawnPoint {
			get {return spawnPoint;}
		}
	}

	public enum SpawnMethod {
		ALL_AT_ONCE,
		RANDOM,
		SEQUENCE
	}

}
