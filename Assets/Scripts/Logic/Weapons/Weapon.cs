﻿using UnityEngine;
using System.Collections;

public class Weapon : MonoBehaviour, IWeapon {

//*** ATTRIBUTES ***//
	[Header("[ Info ]")]
	[SerializeField]
	private string identifier = "unknown";
	
	[Header("[ Base References ]")]
	[SerializeField]
	private GameObject bullet;
	[SerializeField]
	private Transform shootPoint;

	[Header("[ Base Properties ]")]
	[SerializeField]
	private int currentAmmo = 300;
	[SerializeField]
	private int currentClip = 30;

	[Space(10)]
	[SerializeField]
	private int maxAmmo = 300;
	[SerializeField]
	private int clipSize = 30;

	[Header("[ Base Settings ]")]
	[SerializeField]
	private float bulletsPerShot = 1;
	[SerializeField]
	private float fireRate = 10;

	[Space(10)]
	[SerializeField]
	private int reloadTime = 1;
	[SerializeField]
	private float ignitionDelay = 0;

	[Space(10)]
	[SerializeField]
	[Range(0.00f, 1.00f)]
	private float accuracy = 1.0f;

	// EVENTS
	public static System.Action OnWeaponShoot = delegate(){};

//*** PROPERTIES ***//
	// INFO
	public string Identifier {
		get{return identifier;}
	}

	// BASE REFERENCES
	protected GameObject Bullet {
		get{return bullet;}
	}
	protected Transform ShootPoint {
		get{return shootPoint;}
	}
	
	// BASE PROPERTIES
	public int CurrentAmmo {
		get {return currentAmmo;}
		protected set {currentAmmo = value;}
	}
	public int CurrentClip {
		get {return currentClip;}
		protected set {currentClip = value;}
	}

	public int MaxAmmo {
		get{return maxAmmo;}
	}
	public int ClipSize {
		get{return clipSize;}
	}

	// BASE SETTINGS
	public float BulletsPerShot {
		get {return bulletsPerShot;}
	}
	public float FireRate {
		get {return fireRate;}
	}

	public int ReloadTime {
		get{return reloadTime;}
	}
	public float IgnitionDelay {
		get{return ignitionDelay;}
	}

	public float Accuracy {
		get{return accuracy;}
	}

	public bool IsReloading {get; protected set;}
	public bool IsUnlocked {
		get {return Time.time >= TimeToStartShooting;}
	}
	public bool IsInfiniteAmmo {
		get {return MaxAmmo <= 0 || ClipSize <= 0;}
	}

	private float TimeToStartShooting {get; set;}
	private int LastShootingFrame {get; set;}
	private float NextShotTime {get; set;}

//*** METHODS - PUBLIC ***//
	public virtual void Shoot ()
	{
		TryToSetUnlockGunTime();
	}

	public virtual void Reload ()
	{
		if (CurrentAmmo == 0 || CurrentClip == ClipSize)
		{
			return;
		}

		StartCoroutine(ReloadAnimation());
	}

//*** METHODS - PROTECTED ***//
	protected virtual void Awake ()
	{
		Reload();
	}

	protected virtual void Start ()
	{

	}
	
	protected virtual void Update ()
	{

	}
	
	protected virtual bool CanShoot ()
	{
		// weapon lock
		if (IsUnlocked == false || Time.time < NextShotTime)
		{
			return false;
		}

		// weapon is reloading
		if (IsReloading == true && IsInfiniteAmmo == false)
		{
			return false;
		}

		// check if weapon can be reloaded
		if (CurrentClip == 0 && CurrentAmmo > 0 && IsInfiniteAmmo == false)
		{
			Reload();

			return false;
		}

		return true;
	}

	protected virtual void FinalizeShot ()
	{
		CurrentClip = (int)Mathf.MoveTowards(CurrentClip, 0, BulletsPerShot);
		NextShotTime = Time.time + 1.0f / FireRate;

		OnWeaponShoot();
	}

//*** METHODS - PRIVATE ***//
	private void TryToSetUnlockGunTime ()
	{
		if (Time.frameCount - LastShootingFrame > 1)
		{
			TimeToStartShooting = Time.time + IgnitionDelay;
		}

		LastShootingFrame = Time.frameCount;
	}

	private IEnumerator ReloadAnimation ()
	{
		IsReloading = true;
		float targetReloadTime = Time.time + ReloadTime;

		// play the animation
		while (Time.time < targetReloadTime)
		{
			yield return null;
		}

		IsReloading = false;

		// refill ammo
		int ammoToAdd = ClipSize - CurrentClip;

		if (ammoToAdd <= CurrentAmmo)
		{
			CurrentAmmo -= ammoToAdd;
			CurrentClip += ammoToAdd;
		}
		else
		{
			ammoToAdd = CurrentAmmo;

			CurrentAmmo = 0;
			CurrentClip += ammoToAdd;
		}
	}
}
