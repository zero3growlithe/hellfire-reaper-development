﻿using UnityEngine;
using System.Collections;

public class MachineGun : Weapon {

//*** ATTRIBUTES ***//


//*** PROPERTIES ***//


//*** METHODS - PUBLIC ***//
	public override void Shoot()
	{
		base.Shoot();

		if (CanShoot() == false || Bullet == null)
		{
			return;
		}

		Bullet projectile = BulletsManager.Instance.SpawnBullet(Bullet, ShootPoint.position, ShootPoint.rotation);

		// rotate the projectile depending on accuracy
		float maxRotateAngle = 90.0f - 90.0f * Accuracy;
		float randomAngle = Random.Range(-maxRotateAngle, maxRotateAngle);

		projectile.transform.Rotate(0, 0, randomAngle);

		FinalizeShot();
	}

//*** METHODS - PROTECTED ***//
	protected override void Start ()
	{
		base.Start();
	}
	
	protected override void Update ()
	{
		base.Update();
	}

//*** METHODS - PRIVATE ***//

}
