﻿using UnityEngine;
using System.Collections;

public class EnemyMissileLauncher : EnemyGun {

//*** ATTRIBUTES ***//
	

//*** PROPERTIES ***//
	

//*** METHODS - PUBLIC ***//


//*** METHODS - PROTECTED ***//
	protected override void Awake ()
	{
		base.Awake();
	}

	protected override void Start ()
	{
		base.Start();
	}
	
	protected override void Update ()
	{
		base.Update();
	}

	protected override void SpawnBullets ()
	{
		foreach (Transform spawnPoint in SpawnPoints)
		{
			Bullet bullet = SpawnBulletAtPoint(spawnPoint);

			if (GameController.Instance != null)
			{
				((Missile)bullet).SetTarget(GameController.Instance.PlayerShipTransform);
			}
		}
	}

//*** METHODS - PRIVATE ***//

}
