﻿using UnityEngine;
using System.Collections;

public class EnemyGun : MonoBehaviour {

//*** ATTRIBUTES ***//
	[Header("[ References ]")]
	[SerializeField]
	private GameObject bulletToSpawn;
	[SerializeField]
	private Transform[] spawnPoints;

	[Header("[ Settings ]")]
	[SerializeField]
	[Range(0.00f, 1.00f)]
	private float fireProbability = 1.0f;
	[SerializeField]
	private float probabilityDrawRate = 100f;

	[Space(10)]
	[SerializeField]
	private bool canShoot = true;
	[SerializeField]
	private bool aimAtPlayer = false;
	[SerializeField]
	private float fireRate = 10;

//*** PROPERTIES ***//
	// REFERENCES
	protected GameObject BulletToSpawn {
		get {return bulletToSpawn;}
	}
	protected Transform[] SpawnPoints {
		get {return spawnPoints;}
	}

	// SETTINGS
	protected float FireProbability {
		get {return fireProbability;}
	}
	protected float ProbabilityDrawRate {
		get {return probabilityDrawRate;}
	}

	protected bool CanShoot {
		get {return canShoot;}
		set {canShoot = value;}
	}
	protected bool AimAtPlayer {
		get {return aimAtPlayer;}
	}
	protected float FireRate {
		get {return fireRate;}
	}

	protected float NextShootTime {get; set;}
	protected bool WeaponUnlocked {get; set;}

	protected float NextFireDrawTime {get; set;}

	protected Transform PlayerTransform {
		get {return GameController.Instance.PlayerShipTransform;}
	}

//*** METHODS - PUBLIC ***//
	public void EnableShooting (bool state = true)
	{
		CanShoot = state;
	}

	public void DisableShooting ()
	{
		CanShoot = false;
	}

//*** METHODS - PROTECTED ***//
	protected virtual void Awake ()
	{
		
	}

	protected virtual void Start ()
	{
		TryToInvertRotation();
	}
	
	protected virtual void Update ()
	{
		if (CanShoot == false)
		{
			return;
		}

		DrawFireProbability();
		TryShooting();
	}

	protected virtual void TryShooting ()
	{
		if (WeaponUnlocked == false || Time.time < NextShootTime)
		{
			return;
		}

		NextShootTime = Time.time + 1.0f / FireRate;

		SpawnBullets();
	}

	protected virtual void SpawnBullets ()
	{
		if (SpawnPoints.Length == 0)
		{
			SpawnBulletAtPoint(transform);

			return;
		}

		foreach (Transform spawnPoint in SpawnPoints)
		{
			SpawnBulletAtPoint(spawnPoint);
		}
	}

	protected virtual Bullet SpawnBulletAtPoint (Transform spawnPoint)
	{
		Bullet bullet = BulletsManager.Instance.SpawnBullet(BulletToSpawn, spawnPoint.position, spawnPoint.rotation);

		if (AimAtPlayer == true && PlayerTransform != null)
		{
			bullet.transform.Rotate(0, 0, GetAngleToPlayer());
		}

		return bullet;
	}

	protected float GetAngleToPlayer ()
	{
		Vector3 playerDirection = PlayerTransform.position - transform.position;
		float shootAngle = Vector2.Angle(transform.up, playerDirection);
		float side = Vector2.Dot(-transform.right, playerDirection);

		return shootAngle * Mathf.Sign(side);
	}

//*** METHODS - PRIVATE ***//
	// necessary in case of animation mirroring
	private void TryToInvertRotation ()
	{
		Transform currentRoot = transform;
		bool scaleInverted = false;

		while (currentRoot != null)
		{
			scaleInverted |= currentRoot.localScale.x < 0;
			currentRoot = currentRoot.parent;
		}

		if (scaleInverted == false)
		{
			return;
		}

		foreach (Transform point in spawnPoints)
		{
			Vector3 localEuler = point.localEulerAngles;
			
			localEuler.z = 360 - localEuler.z;

			point.localEulerAngles = localEuler;
		}
	}

	private void DrawFireProbability ()
	{
		if (Time.time < NextFireDrawTime)
		{
			return;
		}

		WeaponUnlocked = Random.Range(0.00000f, 1.00000f) < FireProbability;
		NextFireDrawTime = Time.time + 1.0f / ProbabilityDrawRate;
	}
}
