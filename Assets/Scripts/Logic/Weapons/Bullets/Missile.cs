﻿using UnityEngine;
using System.Collections;

public class Missile : Bullet {

//*** ATTRIBUTES ***//
	[Header("[ Extended settings ]")]
	[SerializeField]
	private float bulletSpeed = 20000;
	[SerializeField]
	private float rotateSpeed = 10;
	[SerializeField]
	private float navigationDelay = 1;

//*** PROPERTIES ***//
	private float BulletSpeed {
		get {return bulletSpeed;}
	}
	private float RotateSpeed {
		get {return rotateSpeed;}
	}
	private float NavigationDelay {
		get {return navigationDelay;}
	}

	private Rigidbody2D TargetRigidbody {get; set;}
	private Transform CurrentTarget {get; set;}
	private float NavigationStartTime {get; set;}

//*** METHODS - PUBLIC ***//
	public void SetTarget (Transform newTarget)
	{
		CurrentTarget = newTarget;
	}

	public override void Spawn ()
	{
		base.Spawn();
	}

//*** METHODS - PROTECTED ***//
	protected override void Awake ()
	{
		base.Awake();

		TargetRigidbody = GetComponent<Rigidbody2D>();
	}

	protected override void Start ()
	{
		base.Start();

		NavigationStartTime = Time.time + NavigationDelay;
	}
	
	protected override void Update ()
	{
		base.Update();
		UpdatePosition();
		UpdateRotation();
	}

	protected void OnEnable ()
	{
		NavigationStartTime = Time.time + NavigationDelay;
	}

//*** METHODS - PRIVATE ***//
	private void UpdatePosition ()
	{
		TargetRigidbody.AddRelativeForce(Vector2.up * BulletSpeed * Time.deltaTime);
	}

	private void UpdateRotation ()
	{
		if (CurrentTarget == null || transform.position.y < CurrentTarget.position.y ||
			Time.time < NavigationStartTime)
		{
			return;
		}

		Vector3 targetDirection = CurrentTarget.position - transform.position;
		float rotateDirection = Vector3.Dot(-transform.right, targetDirection);

		transform.Rotate(0, 0, RotateSpeed * Mathf.Sign(rotateDirection) * Time.deltaTime);
	}

}
