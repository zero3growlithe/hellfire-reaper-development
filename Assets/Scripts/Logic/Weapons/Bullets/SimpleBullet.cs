﻿using UnityEngine;
using System.Collections;

public class SimpleBullet : Bullet {

//*** ATTRIBUTES ***//
	[SerializeField]
	private Vector2 bulletSpeed = new Vector2(0, 1000);

//*** PROPERTIES ***//
	private Rigidbody2D TargetRigidbody {get; set;}
	private Vector2 BulletSpeed {
		get {return bulletSpeed;}
		set {bulletSpeed = value;}
	}

//*** METHODS - PUBLIC ***//
	public void Spawn (Vector2 direction)
	{
		BulletSpeed = direction;
	}

//*** METHODS - PROTECTED ***//
	protected override void Awake ()
	{
		base.Awake();

		TargetRigidbody = GetComponent<Rigidbody2D>();
	}

	protected override void Start ()
	{
		base.Start();
	}
	
	protected override void Update ()
	{
		base.Update();
		UpdatePosition();
	}

	protected void OnEnable ()
	{
		
	}

//*** METHODS - PRIVATE ***//
	private void UpdatePosition ()
	{
		TargetRigidbody.AddRelativeForce(BulletSpeed * Time.deltaTime);
	}

}
