﻿using UnityEngine;
using System.Collections;

public class Laser : Bullet {

//*** ATTRIBUTES ***//


//*** PROPERTIES ***//


//*** METHODS - PUBLIC ***//
	public override void Spawn ()
	{
		
	}

	public override void Spawn (Vector3 position, Quaternion rotation)
	{
		
	}

	public override void Destroy ()
	{
		
	}

	public override void Revive ()
	{
		
	}

//*** METHODS - PROTECTED ***//
	protected override void Awake ()
	{
		base.Awake();
	}

	protected override void Start ()
	{
		base.Start();
	}
	
	protected override void Update ()
	{
		base.Update();
	}

	protected override void OnCollisionEnter2D (Collision2D hit)
	{
		GameActions.Instance.NotifyOnBulletHit(new BulletHit(this, hit.gameObject));
	}

//*** METHODS - PRIVATE ***//
	

}
