﻿using UnityEngine;
using System.Collections;

public class Bullet : MonoBehaviour, IDestructable, IBullet {

//*** ATTRIBUTES ***//
	[Header("[ Settings ]")]
	[SerializeField]
	private float lifetime = 5;
	[SerializeField]
	private float cooldownTime = 1;

	[Space(10)]
	[SerializeField]
	private int power = 1;
	[SerializeField]
	private float outOfBoundsOffset = 1.05f;

	[Header("[ References ]")]
	[SerializeField]
	private ParticleSystem explosionParticles;

//*** PROPERTIES ***//
	// SETTINGS
	public float Lifetime {
		get {return lifetime;}
	}
	public float CooldownTime {
		get {return cooldownTime;}
	}

	public int Power {
		get {return power;}
	}
	protected float OutOfBoundsOffset {
		get {return outOfBoundsOffset;}
	}

	// REFERENCES
	protected ParticleSystem ExplosionParticles {
		get {return explosionParticles;}
	}

	// VARIABLES
	public bool CanBeReused {
		get {
			return Time.time > DestroyTime + CooldownTime && isActiveAndEnabled == false;
		}
	}

	protected float DestroyTime {get; set;}

//*** METHODS - PUBLIC ***//
	public virtual void Spawn ()
	{
		Revive();
		Invoke("Destroy", Lifetime);
	}

	public virtual void Spawn (Vector3 position, Quaternion rotation)
	{
		Revive();

		transform.position = position;
		transform.rotation = rotation;

		Invoke("Destroy", Lifetime);
	}

	public virtual void Destroy ()
	{
		if (gameObject.activeSelf == false)
		{
			return;
		}

		DestroyTime = Time.time;

		SetExplosionState(true);
		BulletsManager.Instance.Store(gameObject);
	}

	public virtual void Revive ()
	{
		CancelInvoke("Destroy");

		gameObject.SetActive(true);
		SetExplosionState(false);

		BulletsManager.Instance.Store(gameObject, false);
	}

//*** METHODS - PROTECTED ***//
	protected virtual void Awake ()
	{
		
	}

	protected virtual void Start ()
	{
		
	}
	
	protected virtual void Update ()
	{
		if (CheckIfObjectOutOfScreen() == true)
		{
			Destroy();
		}
	}

	protected virtual void OnCollisionEnter2D (Collision2D hit)
	{
		if (CheckIfObjectOutOfScreen() == false)
		{
			GameActions.Instance.NotifyOnBulletHit(new BulletHit(this, hit.gameObject));
		}

		Destroy();
	}

	protected virtual void OnDestroy ()
	{
		if (ExplosionParticles != null)
		{
			Destroy(ExplosionParticles.gameObject);
		}
	}

//*** METHODS - PRIVATE ***//
	private void SetExplosionState (bool state)
	{
		ExplosionParticles.Stop();

		if ((state == true && CheckIfObjectOutOfScreen() == false) || state == false)
		{
			ExplosionParticles.gameObject.SetActive(state);
		}

		if (state == true)
		{
			ReferenceManager.Instance.StoreTargetInContainer(ExplosionParticles.transform);
		}
		else
		{
			ExplosionParticles.transform.MergeTo(transform);
		}
	}

	private bool CheckIfObjectOutOfScreen ()
	{
		if (Mathf.Abs(transform.position.x) > ScreenCollidersController.Instance.MaxVisibleOffsetWidth * OutOfBoundsOffset ||
			Mathf.Abs(transform.position.y) > ScreenCollidersController.Instance.MaxVisibleOffsetHeight * OutOfBoundsOffset)
		{
			return true;
		}

		return false;
	}

}
