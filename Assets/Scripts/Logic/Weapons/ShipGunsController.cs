﻿using UnityEngine;
using System.Collections;

public class ShipGunsController : MonoBehaviour, IWeapon {

//*** ATTRIBUTES ***//
	[SerializeField]
	private Weapon[] gunsToFire;

//*** PROPERTIES ***//
	private Weapon[] GunsToFire {
		get {return gunsToFire;}
	}	

//*** METHODS - PUBLIC ***//
	public void Shoot ()
	{
		foreach (Weapon gun in GunsToFire)
		{
			gun.Shoot();
		}
	}

//*** METHODS - PROTECTED ***//
	protected virtual void Awake ()
	{
		
	}

	protected virtual void Start ()
	{
		
	}
	
	protected virtual void Update ()
	{
		
	}

//*** METHODS - PRIVATE ***//

}
