﻿using UnityEngine;
using System.Collections;
using System.Collections.Generic;

public class LeaderboardsManager : SaveableManager {

//*** ATTRIBUTES ***//
	[SerializeField]
	private List<PlayerScore> scores;
	[SerializeField]
	private int maxScoreCount = 10;

//*** PROPERTIES ***//
	public static LeaderboardsManager Instance {get; private set;}
	
	private int MaxScoreCount {
		get {return maxScoreCount;}
	}

	public List<PlayerScore> Scores {
		get {return scores;}
		private set {scores = value;}
	}

	public int CurrentScore {get; private set;}
	public int CurrentHighScore {
		get {return GetCurrentHighScore();}
	}

	private ScoreComparer CurrentScoreComparer {get; set;}

//*** METHODS - PUBLIC ***//
	// SCORE MANAGEMENT
	public void AddPoints (int score)
	{
		CurrentScore += score;
	}

	public void SaveNewScore (/*string player*/)
	{
		// temporary hard-coded name
		PlayerScore newScore = new PlayerScore("your score", CurrentScore);

		foreach (PlayerScore score in Scores)
		{
			if (score.Points == newScore.points && score.Name == newScore.name)
			{
				return;
			}
		}

		Scores.Add(newScore);
		Scores.Sort(CurrentScoreComparer);
		Scores.Reverse();

		if (Scores.Count > MaxScoreCount)
		{
			Scores.RemoveAt(Scores.Count - 1);
		}

		CurrentScore = 0;

		GameSettingsManager.Instance.SaveAll();
	}

	// ISTORABLE INTERFACE METHODS
	public override void Save ()
	{
		base.Save();

		SaveDataManager.Instance.Save<List<PlayerScore>>(Scores, SaveFileName);
	}

	public override void Load ()
	{
		base.Load();

		try 
		{
			List<PlayerScore> loadedScores = SaveDataManager.Instance.Load<List<PlayerScore>>(SaveFileName);

			if (loadedScores != null)
			{
				Scores = loadedScores;
			}
		}
		catch (System.Exception e)
		{
			Debug.Log(e);
		}
	}

//*** METHODS - PROTECTED ***//
	protected override void Awake ()
	{
		base.Awake();
		
		Instance = this;
		CurrentScoreComparer = new ScoreComparer();
	}

	protected override void Start ()
	{
		base.Start();

		Load();

		GameActions.OnShipDestroyed += HandleOnShipDestroyedEvent;
		GameActions.OnGameOver += HandleOnGameOverEvent;
	}
	
	protected override void Update ()
	{
		base.Update();	
	}

	protected void OnDestroy ()
	{
		GameActions.OnShipDestroyed -= HandleOnShipDestroyedEvent;
		GameActions.OnGameOver += HandleOnGameOverEvent;
	}

//*** METHODS - PRIVATE ***//
	private int GetCurrentHighScore ()
	{
		if (Scores.Count > 0 && Scores[0].Points > CurrentScore)
		{
			return Scores[0].Points;
		}

		return CurrentScore;
	}

	private void HandleOnShipDestroyedEvent (DestructableShip ship)
	{
		if (ship is EnemyShip)
		{
			AddPoints(((EnemyShip)ship).PointsReward);
		}
	}

	private void HandleOnGameOverEvent (bool isSuccessful)
	{
		SaveNewScore();
	}

}
