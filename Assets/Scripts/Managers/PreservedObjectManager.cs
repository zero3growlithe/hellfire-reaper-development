﻿using UnityEngine;
using System.Collections;
using System.Collections.Generic;

public class PreservedObjectManager : MonoBehaviour {

//*** ATTRIBUTES ***//
	[SerializeField]
	private List<GameObject> objectsToSpawnOnAwake;

//*** PROPERTIES ***//
	public static PreservedObjectManager Instance {get; private set;}
	
	private List<GameObject> ObjectsToSpawnOnAwake {
		get {return objectsToSpawnOnAwake;}
	}

//*** METHODS - PUBLIC ***//


//*** METHODS - PROTECTED ***//
	protected virtual void Awake ()
	{
		DontDestroyOnLoad(gameObject);
		SpawnObjectsCollection();
	}

	protected virtual void Start ()
	{
		
	}
	
	protected virtual void Update ()
	{
		
	}

//*** METHODS - PRIVATE ***//
	private void SpawnObjectsCollection ()
	{
		foreach (GameObject toSpawn in ObjectsToSpawnOnAwake)
		{
			InstantiatePrefab(toSpawn);
		}
	}

	private void InstantiatePrefab (GameObject target)
	{
		Transform spawnedTarget = Instantiate<GameObject>(target).transform;

		spawnedTarget.SetParent(transform);
		spawnedTarget.localPosition = Vector3.zero;
		spawnedTarget.localRotation = Quaternion.identity;

		spawnedTarget.name = target.name;
	}

}
