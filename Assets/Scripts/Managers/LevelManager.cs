﻿using UnityEngine;
using System.Collections;

public class LevelManager : MonoBehaviour {

//*** ATTRIBUTES ***//
	[SerializeField]
	private GameLevel[] levels;

//*** PROPERTIES ***//
	public static LevelManager Instance {get; private set;}

	public GameLevel[] Levels {
		get {return levels;}
	}

	// VARIABLES
	public GameLevel CurrentLevel {get; private set;}
	public GameObject CurrentLevelObject {get; private set;}

//*** METHODS - PUBLIC ***//
	public void SpawnLevelByName (Enums.LevelName name)
	{
		foreach (GameLevel level in Levels)
		{
			if (level.Name == name)
			{
				SpawnLevel(level);
			}
		}
	}

	public void ClearSpawnedLevel ()
	{
		if (CurrentLevelObject != null)
		{
			Destroy(CurrentLevelObject);
		}
	}

//*** METHODS - PROTECTED ***//
	protected virtual void Awake ()
	{
		Instance = this;
	}

	protected virtual void Start ()
	{
		
	}
	
	protected virtual void Update ()
	{
		
	}

//*** METHODS - PRIVATE ***//
	private void SpawnLevel (GameLevel level)
	{
		GameObject spawnedLevel = (GameObject)Instantiate(level.Reference, Vector3.zero, Quaternion.identity);

		CurrentLevel = level;
		CurrentLevelObject = spawnedLevel;
	}

//*** ENUMS ***//
	[System.Serializable]
	public class GameLevel {
		[SerializeField]
		private Enums.LevelName name;
		[SerializeField]
		private GameObject reference;

		public Enums.LevelName Name {
			get {return name;}
		}
		public GameObject Reference {
			get {return reference;}
		}
	}

}
