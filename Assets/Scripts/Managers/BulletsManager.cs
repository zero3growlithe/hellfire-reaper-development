﻿using UnityEngine;
using System.Collections;
using System.Collections.Generic;

public class BulletsManager : MonoBehaviour {

//*** ATTRIBUTES ***//
	

//*** PROPERTIES ***//
	public static BulletsManager Instance {get; private set;}

	private Dictionary<GameObject, List<Bullet>> SpawnedBullets {get; set;}

//*** METHODS - PUBLIC ***//
	public Bullet SpawnBullet(GameObject bullet)
	{
		Bullet cachedBullet = GetCachedBullet(bullet);

		cachedBullet.Spawn();

		return cachedBullet;
	}

	public Bullet SpawnBullet (GameObject bullet, Vector3 position, Quaternion rotation)
	{
		Bullet cachedBullet = GetCachedBullet(bullet);

		cachedBullet.Spawn(position, rotation);

		return cachedBullet;
	}

	public void Store (GameObject bullet, bool disable = true)
	{
		if (disable == true)
		{
			bullet.SetActive(false);
		}
		
		bullet.transform.SetParent(transform);
	}

	public void Restore (Bullet bullet)
	{
		bullet.transform.SetParent(null);
	}

	public void Restore (GameObject bullet)
	{
		bullet.transform.SetParent(null);
	}

	public void DeactivateAllBullets ()
	{
		foreach(KeyValuePair<GameObject, List<Bullet>> keyPair in SpawnedBullets)
		{
			List<Bullet> bullets = SpawnedBullets[keyPair.Key];

			// :'D
			foreach (Bullet bullet in bullets)
			{
				Store(bullet.gameObject);
			}
		}
	}

//*** METHODS - PROTECTED ***//
	protected virtual void Awake ()
	{
		Instance = this;

		SpawnedBullets = new Dictionary<GameObject, List<Bullet>>();
	}

	protected virtual void Start ()
	{
		
	}
	
	protected virtual void Update ()
	{
		
	}

//*** METHODS - PRIVATE ***//
	private Bullet GetCachedBullet (GameObject pattern)
	{
		TryToAddNewBulletPattern(pattern);

		Bullet unusedBullet = GetUnusedBullet(pattern);

		if (unusedBullet != null)
		{
			Restore(unusedBullet);

			return unusedBullet;
		}
		else
		{
			return TryToAddNewCachedBullet(pattern);
		}
	}

	private void TryToAddNewBulletPattern (GameObject pattern)
	{
		if (SpawnedBullets.ContainsKey(pattern) == true)
		{
			return;
		}

		SpawnedBullets.Add(pattern, new List<Bullet>());
	}

	private Bullet TryToAddNewCachedBullet (GameObject bullet)
	{
		List<Bullet> bullets = SpawnedBullets[bullet];
		GameObject newBulletObject = Instantiate<GameObject>(bullet);

		Bullet newBullet = newBulletObject.GetComponent<Bullet>();

		bullets.Add(newBullet);

		return newBullet;
	}

	private Bullet GetUnusedBullet (GameObject pattern)
	{
		List<Bullet> bullets = SpawnedBullets[pattern];

		for (int i = 0; i < bullets.Count; i++)
		{
			if (bullets[i].CanBeReused == true)
			{
				return bullets[i];
			}
		}

		return null;
	}

}
