﻿using UnityEngine;
using System.Collections;
using System.Collections.Generic;

// can execute in edit mode to allow other scripts to access this class's instance
// in edit mode
[ExecuteInEditMode()]
public class ReferenceManager : MonoBehaviour {

//*** ATTRIBUTES ***//
	[Header("[ Resource references ]")]
	[SerializeField]
	private List<ObjectReference> resourceObjects;
	[SerializeField]
	private List<SoundReference> soundEffects;
	[SerializeField]
	private List<MaterialReference> objectMaterials;

	[Header("[ Scene references ]")]
	[SerializeField]
	private string objectContainerName = "DetachedObjectsContainer";

	[Header("[ Layer references ]")]
	[SerializeField]
	private string shipTag = "Ship";

	[Space(10)]
	[SerializeField]
	private int playerShipLayer = 11;
	[SerializeField]
	private int enemyShipLayer = 12;
	[SerializeField]
	private int enemyBulletLayer = 13;
	[SerializeField]
	private int enemyDestructableBulletLayer = 14;

//*** PROPERTIES ***//
	public static ReferenceManager Instance {get; private set;}

	// RESOURCE REFERENCES
	private List<ObjectReference> ResourceObjects {
		get {return resourceObjects;}
	}
	private List<SoundReference> SoundEffects {
		get {return soundEffects;}
	}
	private List<MaterialReference> ObjectMaterials {
		get {return objectMaterials;}
	}

	// SCENE REFERENCES
	private string ObjectContainerName {
		get {return objectContainerName;}
	}

	// LAYER REFERENCES
	public string ShipTag {
		get {return shipTag;}
	}

	// apparently these values need to be ints because of Physics.IgnoreLayerCollision
	public int PlayerShipLayer {
		get {return playerShipLayer;}
	}
	public int EnemyShipLayer {
		get {return enemyShipLayer;}
	}
	public int EnemyBulletLayer {
		get {return enemyBulletLayer;}
	}
	public int EnemyDestructableBulletLayer {
		get {return enemyDestructableBulletLayer;}
	}

	// VARIABLES
	private Transform ObjectContainer {get; set;}

//*** METHODS - PUBLIC ***//
	// SCENE OBJECTS MANAGEMENT
	public void StoreTargetInContainer (Transform target)
	{
		if (ObjectContainer == null)
		{
			ObjectContainer = new GameObject(ObjectContainerName).transform;
		}

		target.SetParent(ObjectContainer);
	}

	// RESOURCE SPAWNING
	public GameObject SpawnSoundEffectByName (string name, Vector3 position, Quaternion rotation)
	{
		return (GameObject)Instantiate(GetSoundEffectByName(name).gameObject, position, rotation);
	}
	
	public GameObject SpawnSoundEffectByName (string name)
	{
		return (GameObject)Instantiate(GetSoundEffectByName(name).gameObject);
	}

	// RESOURCE ACCESS
	public AudioSource GetSoundEffectByName (string name)
	{
		for (int i = 0; i < SoundEffects.Count; i++)
		{
			if (SoundEffects[i].Name == name)
			{
				return SoundEffects[i].TargetAudioSource;
			}
		}

		return null;
	}

	public Material GetObjectMaterialByName (string name)
	{
		for (int i = 0; i < ObjectMaterials.Count; i++)
		{
			if (ObjectMaterials[i].Name == name)
			{
				return ObjectMaterials[i].TargetMaterial;
			}
		}

		return null;
	}


//*** METHODS - PROTECTED ***//
	protected virtual void Awake ()
	{
		Instance = this;
	}

	protected virtual void Start ()
	{
		
	}
	
	protected virtual void Update ()
	{
		
	}

//*** METHODS - PRIVATE ***//


//*** ENUMS ***//
	[System.Serializable]
	public class ObjectReference {
		[SerializeField]
		private string name = "none";
		[SerializeField]
		private GameObject targetGameObject;

		public string Name {
			get {return name;}
		}
		public GameObject TargetGameObject {
			get {return targetGameObject;}
		}
	}

	[System.Serializable]
	public class SoundReference {
		[SerializeField]
		private string name = "none";
		[SerializeField]
		private AudioSource targetAudioSource;

		public string Name {
			get {return name;}
		}
		public AudioSource TargetAudioSource {
			get {return targetAudioSource;}
		}
	}
	
	[System.Serializable]
	public class MaterialReference {
		[SerializeField]
		private string name = "none";
		[SerializeField]
		private Material targetMaterial;

		public string Name {
			get {return name;}
		}
		public Material TargetMaterial {
			get {return targetMaterial;}
		}
	}
}
