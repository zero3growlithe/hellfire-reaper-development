﻿using UnityEngine;
using System.Collections;
using System.Collections.Generic;

public class GameSettingsManager : MonoBehaviour, IStorable {

//*** ATTRIBUTES ***//
	[Header("Game save data")]
	[SerializeField]
	private string saveDataFileName = "slipstreamgx_data.dat";
	[SerializeField]
	private GameSave saveData = new GameSave();

	// TODO: Move these settings to PlayerSettings asset file
	[Header("Game settings")]
	[SerializeField]
	private float defaultScreenWidth = 0.6f;

	[Header("Player settings")]
	[SerializeField]
	private int playerLives = 3;
	[SerializeField]
	private float playerRespawnDelay = 3;

	[Header("Input settings")]
	[SerializeField]
	private string upInputKey = "Up";
	[SerializeField]
	private string downInputKey = "Down";
	[SerializeField]
	private string leftInputKey = "Left";
	[SerializeField]
	private string rightInputKey = "Right";
	[SerializeField]
	private string submitInputKey = "Submit";
	[SerializeField]
	private string escapeInputKey = "Escape";
	[SerializeField]
	private string shootInputKey = "Shoot";

//*** PROPERTIES ***//
	public static GameSettingsManager Instance {get; private set;}

	// GAME SAVE DATA
	public GameSave SaveData {
		get {return saveData;}
		set {saveData = value;}
	}
	private string SaveDataFileName {
		get {return saveDataFileName;}
	}

	// GAME SETTINGS
	public float DefaultScreenWidth {
		get {return defaultScreenWidth;}
	}

	// PLAYER SETTINGS
	public int PlayerLives {
		get {return playerLives;}
	}
	public float PlayerRespawnDelay {
		get {return playerRespawnDelay;}
	}

	// INPUT
	public string UpInputKey {
		get {return upInputKey;}
	}
	public string DownInputKey {
		get {return downInputKey;}
	}
	public string LeftInputKey {
		get {return leftInputKey;}
	}
	public string RightInputKey {
		get {return rightInputKey;}
	}

	public string SubmitInputKey {
		get {return submitInputKey;}
	}
	public string EscapeInputKey {
		get {return escapeInputKey;}
	}

	public string ShootInputKey {
		get {return shootInputKey;}
	}

	// REFERENCES
	private List<IStorable> StorableManagers {get; set;}
	
//*** METHODS - PUBLIC ***//
	public void SaveAll ()
	{
		foreach (IStorable manager in StorableManagers)
		{
			manager.Save();
		}
	}

	public void ResetData ()
	{
		SaveData = new GameSave();
		Save();
	}

	public void Save ()
	{
		SaveDataManager.Instance.Save<GameSave>(SaveData, SaveDataFileName);
	}

	public void Load ()
	{
		try
		{
			SaveData = SaveDataManager.Instance.Load<GameSave>(SaveDataFileName);
		}
		catch (System.Exception e)
		{
			Debug.Log(e);

			SaveData = new GameSave();

			SaveAll();
		}
	}

//*** METHODS - PROTECTED ***//
	protected virtual void Awake ()
	{
		Instance = this;
	}

	protected virtual void Start ()
	{
		Initialize();
	}
	
	protected virtual void Update ()
	{
		
	}

	protected virtual void OnEnable ()
	{
		if (Instance == null)
		{
			Instance = this;
		}
		else if (Instance != this)
		{
			Destroy(gameObject);
		}
	}

//*** METHODS - PRIVATE ***//
	private void Initialize ()
	{
		FindAllStorableObjects();
		Load();
	}

	private void FindAllStorableObjects ()
	{
		Transform[] sceneObjects = FindObjectsOfType<Transform>();
		StorableManagers = new List<IStorable>();

		foreach (Transform foundObject in sceneObjects)
		{
			IStorable manager = foundObject.GetComponent<IStorable>();

			if (manager != null)
			{
				StorableManagers.Add(manager);
			}
		}
	}

}
