﻿using UnityEngine;
using System;
using System.IO;
using System.Collections;
using System.Collections.Generic;
using System.Runtime.Serialization.Formatters.Binary;

public class SaveDataManager : MonoBehaviour {

//*** ATTRIBUTES ***//
	[SerializeField]
	private string saveFolderName = "SlipStream GX";

//*** PROPERTIES ***//
	public static SaveDataManager Instance {get; private set;}
	public string SaveFolderName {
		get {
			return saveFolderName;
		}
	}

	public string DataSavePath {
		get {
			return Application.persistentDataPath + "/" + SaveFolderName;
		}
	}
	private BinaryFormatter Formatter {get; set;}

//*** METHODS - PUBLIC ***//
	// SAVING
	public void Save<T> (T toSave, string filePath, bool customPath = false)
	{
		string fullSavePath = GetGameSavePath(filePath, customPath);

		if (toSave == null)
		{
			Debug.LogWarning("The object you're trying to save is null.");
			return;
		}

		FileStream file = File.Create(fullSavePath);

		Formatter.Serialize(file, toSave);

		file.Close();
	}

	// LOADING
	public T Load<T> (string filePath, bool customPath = false)
	{
		string fullSavePath = GetGameSavePath(filePath, customPath);

		if (File.Exists(fullSavePath) == false)
		{
			Debug.LogWarning("The file you're trying to load doesn't exist.");
			return default(T);
		}

		FileStream file = File.Open(fullSavePath, FileMode.Open);
		T deserializedFile = (T)Formatter.Deserialize(file);

		file.Close();

		return deserializedFile;
	}

//*** METHODS - PROTECTED ***//
	protected virtual void Awake ()
	{
		Instance = this;

		Formatter = new BinaryFormatter();

		CreateSaveFolder();
	}

	protected virtual void Start ()
	{

	}
	
	protected virtual void Update ()
	{

	}

//*** METHODS - PRIVATE ***//
	private void CreateSaveFolder ()
	{
		if (Directory.Exists(DataSavePath) == false)	
		{
			Directory.CreateDirectory(DataSavePath);
		}
	}

	private string GetGameSavePath (string path, bool customPath = false)
	{
		string fullSavePath = DataSavePath + "/" + path;

		if (customPath == true)
		{
			fullSavePath = path;
		}

		return fullSavePath;
	}

}
