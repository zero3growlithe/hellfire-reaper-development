﻿using UnityEngine;
using System.Collections;

public class GameManager : MonoBehaviour {

//*** ATTRIBUTES ***//
	

//*** PROPERTIES ***//
	public static GameManager Instance {get; private set;}

	// SETTINGS
	public bool IsGamePaused {get; private set;}

//*** METHODS - PUBLIC ***//
	public static void LockCursor (bool state = true)
	{
		if (Application.isEditor == true)
		{
			return;
		}

		Cursor.lockState = state ? CursorLockMode.Locked : CursorLockMode.None;
		Cursor.visible = !state;
	}

	public void PauseGame (bool state = true)
	{
		if (IsGamePaused != state)
		{
			GameActions.Instance.NotifyOnGamePauseChanged(state);
		}

		IsGamePaused = state;
		Time.timeScale = (state == true) ? 0.0f : 1.0f;
	}

//*** METHODS - PROTECTED ***//
	protected virtual void Awake ()
	{
		Instance = this;

		Application.targetFrameRate = 60;
	}

	protected virtual void Start ()
	{

	}
	
	protected virtual void Update ()
	{
		HandleGlobalShortcuts();
	}

//*** METHODS - PRIVATE ***//
	private void HandleGlobalShortcuts ()
	{
		
	}
}
