﻿using UnityEngine;
using System.Collections;
using System.Collections.Generic;

public class InputManager : MonoBehaviour {

//*** ATTRIBUTES ***//
	[Header("Default control keys")]
	[SerializeField]
	private InputKey[] controlKeys;

	[Header("Settings")]
	[SerializeField]
	private float keyRepeatDelay = 0.3f;
	[SerializeField]
	private float keyRepeatRate = 8;
	[SerializeField]
	private float axisInputDeadzone = 0.12f;

//*** PROPERTIES ***//
	public static InputManager Instance {get; private set;}

	// DEFAULT CONTROL KEYS
	private InputKey[] ControlKeys {
		get {return controlKeys;}
	}

	// SETTINGS
	private float KeyRepeatDelay {
		get {return keyRepeatDelay;}
	}
	private float KeyRepeatRate {
		get {return keyRepeatRate;}
	}
	private float AxisInputDeadzone {
		get {return axisInputDeadzone;}
	}

	// VARIABLES
	private float KeyRepeaterStart {get; set;}
	private int KeyRepeatCount {get; set;}

	private Dictionary<string, InputAxis> Axes {get; set;}

	// STATIC
	private static readonly string NoneAxisKeyName = "None";

//*** METHODS - PUBLIC ***//
	// TOUCH CONTROLS
	public void SetKeyDown (string key)
	{
		GetInputKeyByName(key).SetState(KeyState.Down);
	}

	public void SetKeyUp (string key)
	{
		GetInputKeyByName(key).SetState(KeyState.Up);
	}

	// KEY STATE GETTERS
	public bool GetKeyDown (string key)
	{
		return GetKeyState(key, KeyState.Down);
	}

	public bool GetKeyUp (string key)
	{
		return GetKeyState(key, KeyState.Up);
	}

	public bool GetKey (string key)
	{
		return GetKeyState(key, KeyState.Hold);
	}

	public bool GetKeyRepeat (string key)
	{
		return GetKeyState(key, KeyState.Repeat);
	}	

//*** METHODS - PROTECTED ***//
	protected virtual void Awake ()
	{
		Instance = this;
		
		CreateAxesDictionary();
	}

	protected virtual void Start ()
	{
		
	}

	protected virtual void Update ()
	{

	}

	protected void LateUpdate ()
	{
		UpdateAxesStates();
	}

//*** METHODS - PRIVATE ***//
	// STATE CHECKS
	private bool GetKeyState (string keyName, KeyState stateCheck)
	{
		InputKey keyInfo = GetInputKeyByName(keyName);
		bool state = false;

		if (keyInfo == null)
		{
			Debug.Log("The following key: " + keyName + " could not be found in InputManager");

			return state;
		}

		// check keys states
		KeyState customKeyState = keyInfo.GetState();
		customKeyState = GetKeyRepeat(
			customKeyState == KeyState.Down,
			customKeyState == KeyState.Hold) ? KeyState.Repeat : customKeyState;

		for (int i = 0; i < keyInfo.KeyList.Length; i++)
		{
			state |= CheckKeyPressByState(keyInfo.KeyList[i], stateCheck);
		}

		state |= CheckAxisInputByState(keyInfo.AxisName, stateCheck, keyInfo.AxisSign);
		state |= (customKeyState == stateCheck);

		return state;
	}

	private bool CheckKeyPressByState (KeyCode key, KeyState state)
	{
		switch (state)
		{
			case KeyState.Hold:
				return Input.GetKey(key);
			case KeyState.Down:
				return Input.GetKeyDown(key);
			case KeyState.Up:
				return Input.GetKeyUp(key);
			case KeyState.Repeat:
				return GetKeyRepeat(Input.GetKeyDown(key), Input.GetKey(key));
		}
		return false;
	}

	private bool CheckAxisInputByState (string axis, KeyState state, AxisSign sign)
	{
		if (axis == NoneAxisKeyName)
		{
			return false;
		}

		switch (state)
		{
			case KeyState.Hold:
				return GetAxis(axis, sign);
			case KeyState.Down:
				return GetAxisDown(axis, sign);
			case KeyState.Up:
				return GetAxisUp(axis, sign);
			case KeyState.Repeat:
				return GetKeyRepeat(GetAxisDown(axis, sign), GetAxis(axis, sign));
		}
		return false;
	}

	private InputKey GetInputKeyByName (string name)
	{
		for (int i = 0; i < ControlKeys.Length; i++)
		{
			if (ControlKeys[i].Name == name)
			{
				return ControlKeys[i];
			}
		}

		Debug.Log("The following key: " + name + " could not be found in InputManager");

		return null;
	}

	private bool GetKeyRepeat (bool isKeyDown, bool isKeyHeld)
	{
		// first key input
		if (isKeyDown == true)
		{
			KeyRepeaterStart = Time.unscaledTime + KeyRepeatDelay;
			KeyRepeatCount = 0;
			return true;
		}

		// wait for the repeater delay
		if (Time.unscaledTime < KeyRepeaterStart || isKeyHeld == false)
		{
			return false;
		}

		// repeat key input
		int currentRepeatCount = (int)Mathf.Floor((Time.unscaledTime - KeyRepeaterStart) * KeyRepeatRate);

		if (KeyRepeatCount != currentRepeatCount)
		{
			KeyRepeatCount = currentRepeatCount;

			return true;
		}

		return false;
	}

	private bool GetAxisDown (string axis, AxisSign sign)
	{
		return GetAxisState(axis, sign) == KeyState.Down;
	}

	private bool GetAxisUp (string axis, AxisSign sign)
	{
		return GetAxisState(axis, sign) == KeyState.Up;
	}

	private bool GetAxis (string axis, AxisSign sign)
	{
		return GetAxisState(axis, sign) == KeyState.Hold;
	}

	private KeyState GetAxisState (string axis, AxisSign sign)
	{
		if (axis == string.Empty)
		{
			return KeyState.None;
		}

		float axisValue = Input.GetAxisRaw(axis);
		float axisAbsValue = Mathf.Abs(axisValue);

		float lastAxisAbsValue = Mathf.Abs(Axes[axis].LastAxisValue);

		// check if axis returned to neutral position
		if (axisAbsValue < AxisInputDeadzone && lastAxisAbsValue > AxisInputDeadzone)
		{
			return KeyState.Up;
		}

		// check if state is unchanged
		if ((axisValue > 0 && sign == AxisSign.Negative) ||
			(axisValue < 0 && sign == AxisSign.Positive))
		{
			return KeyState.None;
		}

		// check if axis is held in one direction
		if (axisAbsValue > AxisInputDeadzone && lastAxisAbsValue > AxisInputDeadzone)
		{
			return KeyState.Hold;
		}

		// check if axis has been just moved from neutral position
		if (axisAbsValue > AxisInputDeadzone && lastAxisAbsValue < AxisInputDeadzone)
		{
			return KeyState.Down;
		}

		return KeyState.None;
	}

	// UPDATED METHODS
	private void UpdateAxesStates ()
	{
		Dictionary<string, InputAxis>.KeyCollection axesKeys = Axes.Keys;

		foreach (string key in axesKeys)
		{
			InputAxis axis = Axes[key];

			if (axis != null)
			{
				axis.UpdateAxisRawValue();
			}
		}
	}

	// SETUP
	private void CreateAxesDictionary ()
	{
		Axes = new Dictionary<string, InputAxis>(){
			{NoneAxisKeyName, null}
		};

		foreach (InputKey key in controlKeys)
		{
			if (key.AxisName != string.Empty && Axes.ContainsKey(key.AxisName) == false)
			{
				Axes.Add(key.AxisName, new InputAxis(key.AxisName));
			}
		}
	}

//*** CLASSES ***//
	[System.Serializable]
	public class InputKey {
	//*** ATTRIBUTES ***//
		[Header("[ General ]")]
		[SerializeField]
		private string name = "unknown";
		[SerializeField]
		private KeyCode[] keyList;

		[Header("[ Axis ]")]
		[SerializeField]
		private string axisName;
		[SerializeField]
		private AxisSign axisSign;

	//*** PROPERTIES ***//
		// GENERAL
		public string Name {
			get {return name;}
		}
		public KeyCode[] KeyList {
			get {return keyList;}
		}

		// AXIS
		public string AxisName {
			get {return axisName;}
		}
		public AxisSign AxisSign {
			get {return axisSign;}
		}

		// VARIABLES (used for on screen buttons)
		public KeyState CurrentState {get; private set;}

		private int LastStateChangeFrame {get; set;}
		private int FramesSinceLastStateChange {
			get {return Time.frameCount - LastStateChangeFrame;}
		}

	//*** METHODS - PUBLIC ***//
		public InputKey ()
		{
			keyList = new KeyCode[1];
		}

		public void SetState (KeyState state)
		{
			if (state != CurrentState)
			{
				LastStateChangeFrame = Time.frameCount;
			}

			CurrentState = state;
		}

		public KeyState GetState ()
		{
			KeyState currentState = KeyState.None;

			// handle key down and hold
			if (CurrentState == KeyState.Down)
			{
				if (FramesSinceLastStateChange == 0)
				{
					currentState = KeyState.Down;
				} else {
					currentState = KeyState.Hold;
				}
			}

			// handle key up and disable
			if (CurrentState == KeyState.Up && FramesSinceLastStateChange == 0)
			{
				currentState = KeyState.Up;
			}

			return currentState;
		}
	}

	public class InputAxis {

	//*** ATTRIBUTES ***//


	//*** PROPERTIES ***//
		public string Name {get; private set;}
		public float LastAxisValue {get; private set;}
		
	//*** METHODS - PUBLIC ***//
		public InputAxis (string name)
		{
			Name = name;
		}

		public void UpdateAxisRawValue ()
		{
			LastAxisValue = Input.GetAxisRaw(Name);
		}
	}

//*** ENUMS ***//
	public enum KeyState
	{
		None,
		Hold,
		Down,
		Up,
		Repeat
	}

	public enum AxisSign
	{
		Positive,
		Negative
	}
}