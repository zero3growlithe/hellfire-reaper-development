﻿using UnityEngine;
using System.Collections;
using System.Collections.Generic;

public class SpawnedJunkManager : MonoBehaviour {

//*** ATTRIBUTES ***//
	

//*** PROPERTIES ***//
	public static SpawnedJunkManager Instance {get; private set;}

	private List<GameObject> SpawnedJunk {get; set;}

//*** METHODS - PUBLIC ***//
	public void AddJunk (GameObject target)
	{
		if (SpawnedJunk.Contains(target) == false)
		{
			SpawnedJunk.Add(target);
		}
	}

	public void DestroyJunk ()
	{
		foreach (GameObject junk in SpawnedJunk)
		{
			if (junk != null)
			{
				Destroy(junk);
			}
		}

		SpawnedJunk = new List<GameObject>();
	}

//*** METHODS - PROTECTED ***//
	protected virtual void Awake ()
	{
		Instance = this;

		SpawnedJunk = new List<GameObject>();
	}

	protected virtual void Start ()
	{
		
	}
	
	protected virtual void Update ()
	{
		
	}

//*** METHODS - PRIVATE ***//

}
