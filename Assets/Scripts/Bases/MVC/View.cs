﻿using UnityEngine;
using System.Collections;

public class View : MonoBehaviour, IView {

//*** ATTRIBUTES ***//


//*** PROPERTIES ***//


//*** METHODS - PUBLIC ***//
	public virtual void Initialize ()
	{
		
	}

//*** METHODS - PROTECTED ***//
	protected virtual void Awake ()
	{
		Initialize();
	}

	protected virtual void Start ()
	{
		
	}
	
	protected virtual void Update ()
	{
		
	}

//*** METHODS - PRIVATE ***//

}
