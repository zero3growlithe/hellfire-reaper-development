﻿using UnityEngine;
using System.Collections;

public class Controller : MonoBehaviour, IController {

//*** ATTRIBUTES ***//
	[Header("[ Base references ]")]
	[SerializeField]
	private Model currentModel;
	[SerializeField]
	private View currentView;

//*** PROPERTIES ***//
	public Model CurrentModel {
		get;
		private set;
	}
	public View CurrentView {
		get;
		private set;
	}

//*** METHODS - PUBLIC ***//
	public virtual void Initialize ()
	{
		CurrentModel = (CurrentModel == null) ? GetComponent<Model>() : CurrentModel;
		CurrentView = (CurrentView == null) ? GetComponent<View>() : CurrentView;

		if (CurrentModel != null)
		{
			CurrentModel.SetCurrentView(CurrentView);
		}
	}

//*** METHODS - PROTECTED ***//
	protected virtual void Awake ()
	{
		Initialize();
	}

	protected virtual void Start ()
	{
		
	}
	
	protected virtual void Update ()
	{
		
	}

//*** METHODS - PRIVATE ***//

}
