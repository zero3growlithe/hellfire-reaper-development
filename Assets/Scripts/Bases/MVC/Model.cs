﻿using UnityEngine;
using System.Collections;

public class Model : MonoBehaviour, IModel {

//*** ATTRIBUTES ***//
	

//*** PROPERTIES ***//
	public View CurrentView {
		get;
		private set;
	}

//*** METHODS - PUBLIC ***//
	public void SetCurrentView (View target)
	{
		CurrentView = target;
	}

	public virtual void Initialize ()
	{
		
	}

//*** METHODS - PROTECTED ***//
	protected virtual void Awake ()
	{
		Initialize();
	}

	protected virtual void Start ()
	{
		
	}
	
	protected virtual void Update ()
	{
		
	}

//*** METHODS - PRIVATE ***//

}
