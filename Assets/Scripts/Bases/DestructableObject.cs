﻿using UnityEngine;
using System.Collections;

public class DestructableObject : MonoBehaviour, IDestructable {

//*** ATTRIBUTES ***//
	

//*** PROPERTIES ***//
	

//*** METHODS - PUBLIC ***//
	public virtual void Destroy ()
	{

	}

	public virtual void Revive ()
	{

	}

//*** METHODS - PROTECTED ***//
	protected virtual void Awake ()
	{
		
	}

	protected virtual void Start ()
	{
		
	}
	
	protected virtual void Update ()
	{
		
	}

//*** METHODS - PRIVATE ***//

}
