﻿using UnityEngine;
using System.Collections;

public class SaveableManager : MonoBehaviour, IStorable {

//*** ATTRIBUTES ***//
	[Header("[ Save settings ]")]
	[SerializeField]
	private string saveFileName = "defaultSaveName";

//*** PROPERTIES ***//
	public string SaveFileName {
		get {return saveFileName;}
	}

//*** METHODS - PUBLIC ***//
	public virtual void Save ()
	{
		
	}

	public virtual void Load ()
	{

	}

//*** METHODS - PROTECTED ***//
	protected virtual void Awake ()
	{
		
	}

	protected virtual void Start ()
	{
		
	}
	
	protected virtual void Update ()
	{
		
	}

//*** METHODS - PRIVATE ***//

}
