﻿using UnityEngine;
using System.Collections;

// WAS SUPPOSED TO MAKE LIFE EASIER BUT MADE IT HARDER INSTEAD
// BECAUSE F***TARD OMNISHARP FOR SUBLIME TEXT 3 CAN'T HANDLE THIS S***
// AND F*** UP THE WHOLE INTELLISENSE
public class SingletonMonoBehaviour<T> : MonoBehaviour where T : Component
{
	//*** ATTRIBUTES ***//


	//*** PROPERTIES ***//
	public static T Instance {get; protected set;}

	//*** METHODS - PUBLIC ***//


	//*** METHODS - PROTECTED ***//
	protected virtual void Awake ()
	{
		if (Instance == null)
		{
			Instance = this as T;
		}
		else if (Instance != this)
		{
			// Debug.Log("A duplicate instance of " + this + " singleton was found and destroyed.");

			Destroy(this);
		}
	}

	//*** METHODS - PRIVATE ***//

}