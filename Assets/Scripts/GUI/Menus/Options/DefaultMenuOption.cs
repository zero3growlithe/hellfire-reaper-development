﻿using UnityEngine;
using UnityEngine.UI;
using System.Collections;

public class DefaultMenuOption : MenuOption {

//*** ATTRIBUTES ***//
	// [Header("[ View ]")]
	// [SerializeField]
	// private Image 

//*** PROPERTIES ***//
	

//*** METHODS - PUBLIC ***//
	public override void Select ()
	{
		base.Select();

		if (CanBeSelected == false)
		{
			return;
		}
	}

	public override void Deselect ()
	{
		base.Deselect();
	}

	public override void SetHighlight (bool state)
	{
		base.SetHighlight(state);
	}

	public override void SetLock (bool state)
	{
		base.SetLock(state);
	}

	public override void SetInteractibility (bool state)
	{
		base.SetInteractibility(state);
	}

//*** METHODS - PROTECTED ***//
	protected override void Awake ()
	{
		base.Awake();
	}

	protected override void Start ()
	{
		base.Start();
	}
	
	protected override void Update ()
	{
		base.Update();
	}

//*** METHODS - PRIVATE ***//

}
