﻿using UnityEngine;
using System;
using System.Collections;
using System.Collections.Generic;

public class MenuController : MonoBehaviour, IHideable {

//*** ATTRIBUTES ***//
	[Header("[ References ]")]
	[SerializeField]
	private MenuScreen initialScreen;

//*** PROPERTIES ***//
	public static MenuController Instance {get; private set;}

	// REFERENCES
	public MenuScreen InitialScreen {
		get {return initialScreen;}
	}

	// SCREEN MANAGEMENT
	public MenuScreen CurrentActiveScreen {get; private set;}
	public List<MenuScreen> AvailableScreens {get; private set;}
	public Stack<MenuScreen> ScreenCallStack {get; private set;}

//*** METHODS - PUBLIC ***//
	public void Activate (bool state = true)
	{
		gameObject.SetActive(state);
	}

	public void Deactivate ()
	{
		gameObject.SetActive(false);
	}

	public void ClearScreenCallStack ()
	{
		ScreenCallStack.Clear();
	}

	public void ReturnToPrevousScreen ()
	{
		MenuScreen previousScreen = (ScreenCallStack.Count > 0) ? ScreenCallStack.Pop() : null;

		SwitchScreen(previousScreen);
	}

	public void GoToNextScreen (Type targetScreen)
	{
		MenuScreen nextScreen = GetScreenByType(targetScreen);

		if (nextScreen == null)
		{
			return;
		}

		ScreenCallStack.Push(CurrentActiveScreen);
		SwitchScreen(nextScreen);
	}

	public void GoToScreen (Type targetScreen)
	{
		MenuScreen nextScreen = GetScreenByType(targetScreen);

		if (nextScreen == null)
		{
			return;
		}

		SwitchScreen(nextScreen);
	}

	public void SwitchScreen (MenuScreen targetScreen)
	{
		if (targetScreen == null)
		{
			return;
		}

		DeactivateAllScreens();
		targetScreen.Activate();

		CurrentActiveScreen = targetScreen;
	}

	public MenuScreen GetScreenByType (Type targetScreen)
	{
		foreach (MenuScreen screen in AvailableScreens)
		{
			if (screen.GetType() == targetScreen)
			{
				return screen;
			}
		}

		return null;
	}

	public void DeactivateAllScreens ()
	{
		foreach (MenuScreen screen in AvailableScreens)
		{
			screen.Deactivate();
		}
	}

//*** METHODS - PROTECTED ***//
	protected virtual void Awake ()
	{
		Instance = this;
		
		InitializeScreens();
	}

	protected virtual void Start ()
	{
		SwitchScreen(InitialScreen);
	}
	
	protected virtual void Update ()
	{
		
	}

//*** METHODS - PRIVATE ***//
	private void InitializeScreens ()
	{
		AvailableScreens = new List<MenuScreen>();
		ScreenCallStack = new Stack<MenuScreen>();
		MenuScreen[] allScreens = Resources.FindObjectsOfTypeAll<MenuScreen>();

		foreach (MenuScreen screen in allScreens)
		{
			screen.Initialize();
			AvailableScreens.Add(screen);
		}
	}

}
