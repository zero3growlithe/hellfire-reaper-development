﻿using UnityEngine;
using UnityEngine.UI;
using System.Collections;
using System.Collections.Generic;

public class MenuGrid : MonoBehaviour {

//*** ATTRIBUTES ***//
	[SerializeField]
	private RectTransform targetGrid;
	[SerializeField]
	private GameObject gridElement;

//*** PROPERTIES ***//
	protected RectTransform TargetGrid {
		get {return targetGrid;}
	}

	protected GameObject GridElement {
		get {return gridElement;}
	}

	protected List<Transform> ProtectedGridElements {
		get;
		set;
	}

//*** METHODS - PUBLIC ***//
	public virtual void UpdateGrid ()
	{

	}

	public virtual void UpdateGrid (List<object> elements)
	{
		
	}

	public virtual void UpdateGrid (object[] elements)
	{

	}

	public virtual void CreateGrid ()
	{
		ClearGrid();
	}

	public virtual void CreateGrid (List<object> elements)
	{
		ClearGrid();
	}

	public virtual void CreateGrid (object[] elements)
	{
		ClearGrid();
	}

	public GameObject SpawnGridElement ()
	{
		GameObject spawnedElement = Instantiate(GridElement);

		SetUpGridElement(spawnedElement);

		return spawnedElement;
	}

	public void ClearGrid ()
	{
		if (TargetGrid == null)
		{
			return;
		}

		// protect disabled children!
		if (ProtectedGridElements == null)
		{
			CreateProtectedElementsList();
		}

		// collect all children
		List<Transform> children = new List<Transform>();

		foreach (Transform child in TargetGrid)
		{
			if (ProtectedGridElements.Contains(child) == false)
			{
				children.Add(child);
			}
		}

		// destroy all children (lel)
		foreach (Transform child in children)
		{
			Destroy(child.gameObject);
		}
	}

	public void SetUpGridElement (GameObject element)
	{
		element.transform.SetParent(TargetGrid);
		element.transform.localScale = Vector3.one;
		element.SetActive(true);
	}

//*** METHODS - PROTECTED ***//
	protected virtual void Awake ()
	{
		GridElement.SetActive(false);
	}

	protected virtual void Start ()
	{
		
	}
	
	protected virtual void Update ()
	{
		
	}

//*** METHODS - PRIVATE ***//
	private void CreateProtectedElementsList ()
	{
		ProtectedGridElements = new List<Transform>();

		foreach (Transform child in TargetGrid)
		{
			ProtectedGridElements.Add(child);
		}
	}

}
