﻿using UnityEngine;
using System.Collections;
using System.Collections.Generic;

public class MenuScreen : MonoBehaviour, IHideable {

//*** ATTRIBUTES ***//
	[Header("[ References ]")]
	[SerializeField]
	private Transform optionsContainer;

	[Header("[ Settings ]")]
	[SerializeField]
	private bool selectOptionOnStart = true;
	[SerializeField]
	private MenuOption optionToSelectOnStart;

//*** PROPERTIES ***//
	// REFERENCES
	private Transform OptionsContainer {
		get {return optionsContainer;}
	}

	// SETTINGS
	private bool SelectOptionOnStart {
		get {return selectOptionOnStart;}
	}
	private MenuOption OptionToSelectOnStart {
		get {return optionToSelectOnStart;}
	}

	// INPUT
	private string UpKey {
		get {return GameSettingsManager.Instance.UpInputKey;}
	}
	private string DownKey {
		get {return GameSettingsManager.Instance.DownInputKey;}
	}
	private string LeftKey {
		get {return GameSettingsManager.Instance.LeftInputKey;}
	}
	private string RightKey {
		get {return GameSettingsManager.Instance.RightInputKey;}
	}

	private string SubmitKey {
		get {return GameSettingsManager.Instance.SubmitInputKey;}
	}
	private string EscapeKey {
		get {return GameSettingsManager.Instance.EscapeInputKey;}
	}

	// VARIABLES
	public static bool SkipInputFrame {get; set;}
	public MenuOption LastSelectedOption {get; private set;}

//*** METHODS - PUBLIC ***//
	public virtual void Activate (bool state = true)
	{
		gameObject.SetActive(state);

		if (SelectOptionOnStart == true && OptionToSelectOnStart != null)
		{
			MenuSelectionController.Instance.SelectOption(OptionToSelectOnStart);

			return;
		}

		if (LastSelectedOption != null)
		{
			MenuSelectionController.Instance.SelectOption(LastSelectedOption);
		}
		else
		{
			MenuSelectionController.Instance.RecoverSelection();
		}
	}

	public virtual void Deactivate ()
	{
		gameObject.SetActive(false);
	}

	public void Initialize ()
	{
		SetUpOptions();
	}

//*** METHODS - PROTECTED ***//
	protected virtual void Awake ()
	{
		
	}

	protected virtual void Start ()
	{
		
	}
	
	protected virtual void Update ()
	{
		HandleInput();
	}
	
	protected virtual void OnDestroy ()
	{

	}
	
	protected virtual void OnEnable ()
	{
		AttachToEvents();
	}
	
	protected virtual void OnDisable ()
	{
		DetachFromEvents();
	}

	protected virtual void AttachToEvents ()
	{
		MenuSelectionController.Instance.OnOptionSelect += HandleOnOptionSelect;
		MenuSelectionController.Instance.OnOptionExecute += HandleOnOptionExecute;
	}

	protected virtual void DetachFromEvents ()
	{
		MenuSelectionController.Instance.OnOptionSelect -= HandleOnOptionSelect;
		MenuSelectionController.Instance.OnOptionExecute -= HandleOnOptionExecute;
	}

	// EVENTS HANDLING
	protected virtual void HandleOnOptionSelect (MenuOption option)
	{
		if (CheckIfOptionIsChildOfScreen(option) == false)
		{
			return;
		}
	}

	protected virtual void HandleOnOptionExecute (MenuOption option)
	{
		if (option != null && CheckIfOptionIsChildOfScreen(option) == true)
		{
			LastSelectedOption = MenuSelectionController.Instance.CurrentSelectedOption;
		}
	}

	// TOOLS
	protected bool CheckIfOptionIsChildOfScreen (MenuOption option)
	{
		Transform currentCheck = option.transform;

		while (currentCheck != null)
		{
			if (currentCheck == transform)
			{
				return true;
			}

			currentCheck = currentCheck.parent;
		}

		return false;
	}

//*** METHODS - PRIVATE ***//
	private void HandleInput ()
	{
		// lock input if not in menu
		if (GameController.Instance.CurrentGameState != Enums.GameState.MAIN_MENU &&
			GameController.Instance.CurrentGameState != Enums.GameState.PAUSE)
		{
			return;
		}

		// handle navigation
		if (InputManager.Instance.GetKeyRepeat(UpKey) == true)
		{
			MenuSelectionController.Instance.SelectUp();
		}

		if (InputManager.Instance.GetKeyRepeat(DownKey) == true)
		{
			MenuSelectionController.Instance.SelectDown();
		}

		// handle submit button
		if (InputManager.Instance.GetKeyDown(SubmitKey) == true && SkipInputFrame == false)
		{
			SkipInputFrame = true;
			MenuSelectionController.Instance.ExecuteOption();
		}
		else if (InputManager.Instance.GetKeyUp(SubmitKey) == true)
		{
			SkipInputFrame = false;
		}

		// handle escape button
		if (InputManager.Instance.GetKeyDown(EscapeKey) == true &&
			GameController.Instance.CurrentGameState == Enums.GameState.MAIN_MENU)
		{
			MenuController.Instance.ReturnToPrevousScreen();
		}
	}

	private void SetUpOptions ()
	{
		List<MenuOption> options = GetOptionsList();

		// shitty exceptional cases
		if (options.Count == 2)
		{
			options[0].OverrideSelectionReferences(options[1], options[1], null, null);
			options[1].OverrideSelectionReferences(options[0], options[0], null, null);
		}
		else if (options.Count == 1)
		{
			return;
		}

		for (int i = 0; i < options.Count; i++)
		{
			// set selection references
			if (i == 0)
			{
				options[i].OverrideSelectionReferences(options[options.Count - 1], options[i + 1], null, null);
			}
			else if (i == options.Count - 1)
			{
				options[i].OverrideSelectionReferences(options[i - 1], options[0], null, null);
			}
			else
			{
				options[i].OverrideSelectionReferences(options[i - 1], options[i + 1], null, null);
			}

			// set option as first selection
			if (options[i].IsSelected == true)
			{
				LastSelectedOption = options[i];
			}
		}
	}

	private List<MenuOption> GetOptionsList ()
	{
		if (OptionsContainer == null)
		{
			return new List<MenuOption>();
		}

		List<MenuOption> output = new List<MenuOption>();

		foreach (Transform child in OptionsContainer)
		{
			MenuOption option = child.GetComponent<MenuOption>();

			if (option == null)
			{
				continue;
			}

			output.Add(option);
		}

		return output;
	}
}
