﻿using UnityEngine;
using UnityEngine.UI;
using UnityEngine.EventSystems;
using System.Collections;

public class MenuOption : MonoBehaviour {

//*** ATTRIBUTES ***//
	[Header("[ References ]")]
	[ContextMenuItemAttribute("Setup button event", "SetupButtonEvent")]
	[SerializeField]
	private Button targetButton;

	[Header("[ Settings ]")]
	[SerializeField]
	private bool isSelected = false;
	[SerializeField]
	private bool canBeSelected = true;
	[SerializeField]
	private bool isLocked = false;

	[Header("[ Selection ]")]
	[SerializeField]
	private bool canBeOverridenFromCode = true;
	[SerializeField]
	private MenuOption topOption;
	[SerializeField]
	private MenuOption bottomOption;
	[SerializeField]
	private MenuOption leftOption;
	[SerializeField]
	private MenuOption rightOption;

//*** PROPERTIES ***//
	// REFERENCES
	public Button TargetButton {
		get {return targetButton;}
		protected set {targetButton = value;}
	}

	// SETTINGS
	public bool IsSelected {
		get {return isSelected;}
		protected set {isSelected = value;}
	}
	public bool CanBeSelected {
		get {return canBeSelected && isActiveAndEnabled;}
		protected set {canBeSelected = value;}
	}
	public bool IsLocked {
		get {return isLocked;}
		protected set {isLocked = value;}
	}
	protected bool IsHighlighted {get; set;}

	// SELECTION
	public MenuOption TopOption {
		get {return topOption;}
		protected set {topOption = value;}
	}
	public MenuOption BottomOption {
		get {return bottomOption;}
		protected set {bottomOption = value;}
	}
	public MenuOption LeftOption {
		get {return leftOption;}
		protected set {leftOption = value;}
	}
	public MenuOption RightOption {
		get {return rightOption;}
		protected set {rightOption = value;}
	}
	protected bool CanBeOverridenFromCode {
		get {return canBeOverridenFromCode;}
	}

//*** METHODS - PUBLIC ***//
	// SELECTION CONTROLS
	public virtual void Execute ()
	{
		Select();
		
		if (IsSelected == false || IsLocked == true)
		{
			return;
		}

		MenuSelectionController.Instance.NotifyOnOptionExecute(this);
	}

	public virtual void SingleSelect ()
	{
		MenuSelectionController.Instance.SelectOption(this);
	}

	public virtual void Select ()
	{
		if (CanBeSelected == false)
		{
			return;
		}

		IsSelected = true;

		SetHighlight(true);
		EventSystem.current.SetSelectedGameObject(null);
		TargetButton.Select();
		MenuSelectionController.Instance.NotifyOnOptionSelect(this);
	}

	public virtual void Deselect ()
	{
		SetHighlight (false);

		IsSelected = false;
	}

	public virtual void SetHighlight (bool state)
	{
		IsHighlighted = state;
	}

	public virtual void SetLock (bool state)
	{
		IsLocked = state;

		UpdateTargetButton();
	}

	public virtual void SetInteractibility (bool state)
	{
		CanBeSelected = state;
	}

	// REFERENCE CONTROLS
	public void ClearSelectionReferences ()
	{
		if (CanBeOverridenFromCode == false)
		{
			Debug.Log(gameObject + " cannot have its references overriden. It is locked.", gameObject);
			return;
		}

		TopOption = null;
		BottomOption = null;
		LeftOption = null;
		RightOption = null;
	}

	public void OverrideSelectionReferences (MenuOption top, MenuOption bottom, MenuOption left, MenuOption right)
	{
		if (CanBeOverridenFromCode == false)
		{
			Debug.Log(gameObject + " cannot have its references overriden. It is locked.", gameObject);
			return;
		}

		TopOption = (top != null) ? top : TopOption;
		BottomOption = (bottom != null) ? bottom : BottomOption;
		LeftOption = (left != null) ? left : LeftOption;
		RightOption = (right != null) ? right : RightOption;
	}

//*** METHODS - PROTECTED ***//
	protected virtual void Awake ()
	{
		TargetButton = (TargetButton == null) ? GetComponent<Button>() : TargetButton;
	}

	protected virtual void Start ()
	{
		UpdateTargetButton();
	}
	
	protected virtual void Update ()
	{
		
	}

	protected virtual void OnEnable ()
	{
		MenuSelectionController.Instance.AddOptionToActive(this);
	}

	protected virtual void OnDisable ()
	{
		MenuSelectionController.Instance.RemoveOptionFromActive(this);
	}

	protected void SetupButtonEvent ()
	{
		TargetButton = (TargetButton == null) ? GetComponent<Button>() : TargetButton;

		if (TargetButton == null)
		{
			return;
		}

		TargetButton.onClick.RemoveAllListeners();
		TargetButton.onClick.AddListener(() => Execute());
	}

//*** METHODS - PRIVATE ***//
	private void UpdateTargetButton ()
	{
		TargetButton.interactable = !IsLocked;
	}

//*** ENUMS ***//

}
