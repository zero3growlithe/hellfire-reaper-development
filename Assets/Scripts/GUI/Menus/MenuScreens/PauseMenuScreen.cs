﻿using UnityEngine;
using System.Collections;

public class PauseMenuScreen : MenuScreen {

//*** ATTRIBUTES ***//
	

//*** PROPERTIES ***//
	

//*** METHODS - PUBLIC ***//


//*** METHODS - PROTECTED ***//
	protected override void Awake ()
	{
		base.Awake();
	}

	protected override void Start ()
	{
		base.Start();
	}
	
	protected override void Update ()
	{
		base.Update();
	}

	protected override void HandleOnOptionSelect (MenuOption option)
	{
		base.HandleOnOptionSelect(option);
	}

	protected override void HandleOnOptionExecute (MenuOption option)
	{
		base.HandleOnOptionExecute(option);

		if (CheckIfOptionIsChildOfScreen(option) == false)
		{
			return;
		}

		switch (option.name)
		{
			case "Continue":
				GameController.Instance.TogglePause();
				break;
			case "Quit":
				GameController.Instance.QuitGame();
				break;
		}
	}

//*** METHODS - PRIVATE ***//

}
