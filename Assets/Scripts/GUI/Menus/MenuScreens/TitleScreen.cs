﻿using UnityEngine;
using System.Collections;

public class TitleScreen : MenuScreen {

//*** ATTRIBUTES ***//
	

//*** PROPERTIES ***//
	

//*** METHODS - PUBLIC ***//
	public void GoToMainMenu ()
	{

	}

//*** METHODS - PROTECTED ***//
	protected override void Awake ()
	{
		base.Awake();
	}

	protected override void Start ()
	{
		base.Start();
	}
	
	protected override void Update ()
	{
		base.Update();
	}

	protected override void HandleOnOptionSelect (MenuOption option)
	{
		base.HandleOnOptionSelect(option);
	}

	protected override void HandleOnOptionExecute (MenuOption option)
	{
		base.HandleOnOptionExecute(option);

		if (CheckIfOptionIsChildOfScreen(option) == false)
		{
			return;
		}

		MenuController.Instance.GoToNextScreen(typeof(MainMenuScreen));
	}

//*** METHODS - PRIVATE ***//

}
