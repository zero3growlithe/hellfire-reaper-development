﻿using UnityEngine;
using System.Collections;

public class MainMenuScreen : MenuScreen {

//*** ATTRIBUTES ***//
	

//*** PROPERTIES ***//
	

//*** METHODS - PUBLIC ***//


//*** METHODS - PROTECTED ***//
	protected override void Awake ()
	{
		base.Awake();
	}

	protected override void Start ()
	{
		base.Start();
	}
	
	protected override void Update ()
	{
		base.Update();
	}

	protected override void HandleOnOptionSelect (MenuOption option)
	{
		base.HandleOnOptionSelect(option);
	}

	protected override void HandleOnOptionExecute (MenuOption option)
	{
		base.HandleOnOptionExecute(option);

		if (CheckIfOptionIsChildOfScreen(option) == false)
		{
			return;
		}

		switch (option.name)
		{
			case "StartGame":
				GameGUIController.Instance.StartGameStartAnimation();
				break;
			case "Options":
				break;
			case "Scores":
				MenuController.Instance.GoToNextScreen(typeof(ScoresScreen));
				break;
			case "Exit":
				Application.Quit();
				break;
		}
	}

//*** METHODS - PRIVATE ***//

}
