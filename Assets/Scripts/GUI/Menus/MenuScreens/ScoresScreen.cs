﻿using UnityEngine;
using System.Collections;

public class ScoresScreen : MenuScreen {

//*** ATTRIBUTES ***//
	[SerializeField]
	private MenuGrid scoresGrid;

//*** PROPERTIES ***//
	private MenuGrid ScoresGrid {
		get {return scoresGrid;}
	}

//*** METHODS - PUBLIC ***//
	public override void Activate (bool state = true)
	{
		base.Activate(state);

		ScoresGrid.CreateGrid();
	}

//*** METHODS - PROTECTED ***//

	protected override void Awake ()
	{
		base.Awake();
	}

	protected override void Start ()
	{
		base.Start();
	}
	
	protected override void Update ()
	{
		base.Update();
	}

	protected override void HandleOnOptionSelect (MenuOption option)
	{
		base.HandleOnOptionSelect(option);
	}

	protected override void HandleOnOptionExecute (MenuOption option)
	{
		base.HandleOnOptionExecute(option);

		if (CheckIfOptionIsChildOfScreen(option) == false)
		{
			return;
		}

		switch (option.name)
		{
			case "Return":
				MenuController.Instance.ReturnToPrevousScreen();
				break;
		}
	}

//*** METHODS - PRIVATE ***//

}
