﻿using UnityEngine;
using UnityEngine.UI;
using UnityEngine.EventSystems;
using System;
using System.Collections;
using System.Collections.Generic;

public class MenuSelectionController : MonoBehaviour {

//*** ATTRIBUTES ***//
	public Action<MenuOption> OnOptionSelect;
	public Action<MenuOption> OnOptionExecute;

//*** PROPERTIES ***//
	public static MenuSelectionController Instance {get; private set;}

	public List<MenuOption> CurrentActiveOptions {get; private set;}
	public MenuOption CurrentSelectedOption {get; private set;}
	public MenuOption LastSelectedOption {get; private set;}

//*** METHODS - PUBLIC ***//
	// OPTIONS CONTROLS
	public bool RecoverSelection ()
	{
		if (CurrentSelectedOption != null && CurrentActiveOptions.Contains(CurrentSelectedOption))
		{
			if (CurrentSelectedOption.gameObject == EventSystem.current.currentSelectedGameObject)
			{
				return false;
			}
			else
			{
				EventSystem.current.SetSelectedGameObject(CurrentSelectedOption.gameObject);

				return true;	
			}
		}

		if (LastSelectedOption != null && CurrentActiveOptions.Contains(LastSelectedOption))
		{
			SelectOption(LastSelectedOption);
		}
		else if (CurrentActiveOptions.Count > 0)
		{
			SelectOption(CurrentActiveOptions[0]);
		}

		return true;
	}

	public void SelectUp ()
	{
		if (RecoverSelection() == false)
		{
			SelectOptionInDirection(Direction.UP);
		}
	}

	public void SelectDown ()
	{
		if (RecoverSelection() == false)
		{
			SelectOptionInDirection(Direction.DOWN);
		}
	}

	public void SelectLeft ()
	{
		if (RecoverSelection() == false)
		{
			SelectOptionInDirection(Direction.LEFT);
		}
	}

	public void SelectRight ()
	{
		if (RecoverSelection() == false)
		{
			SelectOptionInDirection(Direction.RIGHT);
		}
	}

	public void SelectOptionInDirection (Direction targetDirection)
	{
		if (CurrentSelectedOption == null)
		{
			return;
		}

		MenuOption optionToSelect = CurrentSelectedOption;

		// select option in direction
		while (optionToSelect != LastSelectedOption)
		{
			switch (targetDirection)
			{
				case Direction.UP:
					optionToSelect = optionToSelect.TopOption;
					break;
				case Direction.DOWN:
					optionToSelect = optionToSelect.BottomOption;
					break;
				case Direction.LEFT:
					optionToSelect = optionToSelect.LeftOption;
					break;
				case Direction.RIGHT:
					optionToSelect = optionToSelect.RightOption;
					break;
			}

			// repeat if option is locked
			if (optionToSelect == null || optionToSelect.CanBeSelected == true)
			{
				break;
			}
		}

		SelectOption(optionToSelect);
	}

	public void SelectOption (MenuOption option)
	{
		DeselectAllActiveOptions();

		if (option == null || CurrentActiveOptions.Contains(option) == false)
		{
			return;
		}

		option.Select();
	}

	public void HighlightOption (MenuOption option)
	{
		UnhighlightAllActiveOptions();

		if (option == null || CurrentActiveOptions.Contains(option) == false)
		{
			return;
		}

		option.SetHighlight(true);
	}

	public void ExecuteOption ()
	{
		if (CurrentSelectedOption == null || CurrentSelectedOption.isActiveAndEnabled == false)
		{
			return;
		}

		CurrentSelectedOption.Execute();
	}

	public void ExecuteOption (MenuOption option)
	{
		if (option.IsSelected == false)
		{
			option.Select();
		}

		option.Execute();
	}

	public void DeselectAllActiveOptions ()
	{
		foreach (MenuOption option in CurrentActiveOptions)
		{
			option.Deselect();
		}
	}

	public void UnhighlightAllActiveOptions ()
	{
		foreach (MenuOption option in CurrentActiveOptions)
		{
			if (option.IsSelected == false)
			{
				option.SetHighlight(false);
			}
		}
	}

	public void AddOptionToActive (MenuOption option)
	{
		CurrentActiveOptions.Add(option);
	}

	public void RemoveOptionFromActive (MenuOption option)
	{
		option.Deselect();
		CurrentActiveOptions.Remove(option);
	}

	// EVENT CONTROLS
	public void NotifyOnOptionSelect (MenuOption option)
	{
		if (OnOptionSelect != null)
		{
			OnOptionSelect(option);
		}
	}

	public void NotifyOnOptionExecute (MenuOption option)
	{
		if (OnOptionExecute != null)
		{
			OnOptionExecute(option);
		}
	}

//*** METHODS - PROTECTED ***//
	protected virtual void Awake ()
	{
		if (Instance == null)
		{
			Instance = this;
		}
		else if (this != Instance)
		{
			Destroy(gameObject);
		}

		CurrentActiveOptions = new List<MenuOption>();
	}

	protected virtual void Start ()
	{
		AttachToEvents();
	}
	
	protected virtual void Update ()
	{
		
	}
	
	protected virtual void OnDestroy ()
	{
		DetachFromEvents();
	}

	protected virtual void AttachToEvents ()
	{
		OnOptionSelect += SetCurrentSelectedOption;
	}

	protected virtual void DetachFromEvents ()
	{
		OnOptionSelect -= SetCurrentSelectedOption;
	}

//*** METHODS - PRIVATE ***//
	private void SetCurrentSelectedOption (MenuOption option)
	{
		if (option != CurrentSelectedOption)
		{
			LastSelectedOption = CurrentSelectedOption;
		}

		CurrentSelectedOption = option;
	}

//*** ENUMS ***//
	public enum Direction {
		UP,
		DOWN,
		LEFT,
		RIGHT
	}

}
