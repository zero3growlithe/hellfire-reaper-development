﻿using UnityEngine;
using UnityEngine.UI;
using System.Collections;

public class ScoreGridEntry : MonoBehaviour {

//*** ATTRIBUTES ***//
	[SerializeField]
	private Text playerNameLabel;
	[SerializeField]
	private Text scoreLabel;

//*** PROPERTIES ***//
	private Text PlayerNameLabel {
		get {return playerNameLabel;}
	}
	private Text ScoreLabel {
		get {return scoreLabel;}
	}

//*** METHODS - PUBLIC ***//
	public void SetEntryData (string player, int score)
	{
		PlayerNameLabel.text = player;
		ScoreLabel.text = score.ToString();
	}

//*** METHODS - PROTECTED ***//
	protected virtual void Awake ()
	{
		
	}

	protected virtual void Start ()
	{
		
	}
	
	protected virtual void Update ()
	{
		
	}

//*** METHODS - PRIVATE ***//

}
