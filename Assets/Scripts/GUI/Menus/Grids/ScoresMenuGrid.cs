﻿using UnityEngine;
using System.Collections;
using System.Collections.Generic;

public class ScoresMenuGrid : MenuGrid {

//*** ATTRIBUTES ***//
	

//*** PROPERTIES ***//
	

//*** METHODS - PUBLIC ***//
	public override void CreateGrid ()
	{
		base.CreateGrid();

		List<PlayerScore> scores = LeaderboardsManager.Instance.Scores;

		for (int i = 0; i < scores.Count; i++)
		{
			ScoreGridEntry gridEntry = SpawnGridElement().GetComponent<ScoreGridEntry>();

			gridEntry.SetEntryData(scores[i].Name, scores[i].Points);
		}
	}

//*** METHODS - PROTECTED ***//
	protected override void Awake ()
	{
		base.Awake();
	}

	protected override void Start ()
	{
		base.Start();
	}
	
	protected override void Update ()
	{
		base.Update();
	}

//*** METHODS - PRIVATE ***//

}
