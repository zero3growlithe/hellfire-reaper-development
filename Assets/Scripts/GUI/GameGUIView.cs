﻿using UnityEngine;
using System.Collections;

public class GameGUIView : View {

//*** ATTRIBUTES ***//
	[Header("[ References ]")]
	[SerializeField]
	private MenuController gameMenu; 
	[SerializeField]
	private HUDController gameHUD;
	[SerializeField]
	private GameObject bossHealth;

	[Header("[ Animation ]")]
	[SerializeField]
	private Animator gameStartAnimation;
	[SerializeField]
	private Animator bossWarningAnimation;
	[SerializeField]
	private Animator gameOverAnimation;

//*** PROPERTIES ***//
	// REFERENCES
	private MenuController GameMenu {
		get {return gameMenu;}
	}
	private HUDController GameHUD {
		get {return gameHUD;}
	}
	private GameObject BossHealth {
		get {return bossHealth;}
	}

	// ANIMATION
	private Animator GameStartAnimation {
		get {return gameStartAnimation;}
	}
	private Animator BossWarningAnimation {
		get {return bossWarningAnimation;}
	}
	private Animator GameOverAnimation {
		get {return gameOverAnimation;}
	}

//*** METHODS - PUBLIC ***//
	public void SetMenuVisibilityState (bool state)
	{
		GameMenu.Activate(state);
	}

	public void SetHUDVisibilityState (bool state)
	{
		GameHUD.Activate(state);
	}

	public void SetBossHealthVisibility (bool state)
	{
		BossHealth.SetActive(state);
	}

	public void StartGameStartAnimation ()
	{
		GameStartAnimation.enabled = true;
		GameStartAnimation.gameObject.SetActive(true);
	}

	public void StartBossWarningAnimation ()
	{
		BossWarningAnimation.gameObject.SetActive(true);
	}

	public void StartGameOverAnimation ()
	{
		GameOverAnimation.gameObject.SetActive(true);
	}

//*** METHODS - PROTECTED ***//
	protected override void Awake ()
	{
		base.Awake();

		GameStartAnimation.enabled = false;
	}

	protected override void Start ()
	{
		base.Start();
	}
	
	protected override void Update ()
	{
		base.Update();
	}

//*** METHODS - PRIVATE ***//

}
