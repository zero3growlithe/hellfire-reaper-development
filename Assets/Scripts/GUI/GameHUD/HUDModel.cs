﻿using UnityEngine;
using System.Collections;

public class HUDModel : Model {

//*** ATTRIBUTES ***//
	[SerializeField]
	private ClassValue playerScoreValue;
	[SerializeField]
	private ClassValue playerHighScoreValue;
	[SerializeField]
	private ClassValue playerLivesValue;

//*** PROPERTIES ***//
	private ClassValue PlayerScoreValue {
		get {return playerScoreValue;}
	}
	private ClassValue PlayerHighScoreValue {
		get {return playerHighScoreValue;}
	}
	private ClassValue PlayerLivesValue {
		get {return playerLivesValue;}
	}

//*** METHODS - PUBLIC ***//


//*** METHODS - PROTECTED ***//
	protected override void Awake ()
	{
		base.Awake();	
	}

	protected override void Start ()
	{
		base.Start();	
	}
	
	protected override void Update ()
	{
		base.Update();

		UpdateHUD();
	}

//*** METHODS - PRIVATE ***//
	private void UpdateHUD ()
	{
		((HUDView)CurrentView).SetPlayerScore(PlayerScoreValue.GetIntValue());
		((HUDView)CurrentView).SetPlayerHighScore(PlayerHighScoreValue.GetIntValue());
		((HUDView)CurrentView).SetPlayerLives(PlayerLivesValue.GetIntValue());
	}

}
