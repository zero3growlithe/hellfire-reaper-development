﻿using UnityEngine;
using UnityEngine.UI;
using System.Collections;

public class HUDView : View {

//*** ATTRIBUTES ***//
	[Header("[ References ]")]
	[SerializeField]
	private Text scoreLabel;
	[SerializeField]
	private Text highScoreLabel;
	[SerializeField]
	private PlayerLivesGUI playerLivesController;

	[Header("[ Settings ]")]
	[SerializeField]
	private string scoreFormat = "SCORE {0}";
	[SerializeField]
	private string highScoreFormat = "HIGH {0}";

//*** PROPERTIES ***//
	private Text ScoreLabel {
		get {return scoreLabel;}
	}
	private Text HighScoreLabel {
		get {return highScoreLabel;}
	}
	private PlayerLivesGUI PlayerLivesController {
		get {return playerLivesController;}
	}

	private string ScoreFormat {
		get {return scoreFormat;}
	}
	private string HighScoreFormat {
		get {return highScoreFormat;}
	}

//*** METHODS - PUBLIC ***//
	public void SetPlayerLives (int count)
	{
		PlayerLivesController.SetActiveLives(count);
	}

	public void SetPlayerScore (int score)
	{
		ScoreLabel.text = string.Format(ScoreFormat, score);
	}

	public void SetPlayerHighScore (int score)
	{
		HighScoreLabel.text = string.Format(HighScoreFormat, score);
	}

//*** METHODS - PROTECTED ***//
	protected override void Awake ()
	{
		base.Awake();	
	}

	protected override void Start ()
	{
		base.Start();	
	}
	
	protected override void Update ()
	{
		base.Update();	
	}

//*** METHODS - PRIVATE ***//

}
