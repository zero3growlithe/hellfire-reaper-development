﻿using UnityEngine;
using System.Collections;

public class PlayerLivesGUI : MonoBehaviour {

//*** ATTRIBUTES ***//
	[SerializeField]
	private GameObject[] lifeIcons;

//*** PROPERTIES ***//
	private GameObject[] LifeIcons {
		get {return lifeIcons;}
	}

//*** METHODS - PUBLIC ***//
	public void SetActiveLives (int count)
	{
		if (count < 0 || count >= LifeIcons.Length)
		{
			return;
		}

		for (int i = 0; i < LifeIcons.Length; i++)
		{
			LifeIcons[i].SetActive(i < count);
		}
	}

//*** METHODS - PROTECTED ***//
	protected virtual void Awake ()
	{
		
	}

	protected virtual void Start ()
	{
		
	}
	
	protected virtual void Update ()
	{
		
	}

//*** METHODS - PRIVATE ***//

}
