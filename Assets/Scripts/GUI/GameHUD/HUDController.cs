﻿using UnityEngine;
using System.Collections;

public class HUDController : Controller, IHideable {

//*** ATTRIBUTES ***//
	

//*** PROPERTIES ***//
	

//*** METHODS - PUBLIC ***//
	public void Activate (bool state = true)
	{
		gameObject.SetActive(state);
	}

	public void Deactivate ()
	{
		gameObject.SetActive(false);
	}

//*** METHODS - PROTECTED ***//
	protected override void Awake ()
	{
		base.Awake();	
	}

	protected override void Start ()
	{
		base.Start();	
	}
	
	protected override void Update ()
	{
		base.Update();	
	}

//*** METHODS - PRIVATE ***//

}
