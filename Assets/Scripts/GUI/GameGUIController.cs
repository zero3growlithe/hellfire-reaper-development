﻿using UnityEngine;
using System.Collections;

public class GameGUIController : Controller {

//*** ATTRIBUTES ***//
	

//*** PROPERTIES ***//
	public static GameGUIController Instance {get; private set;}

//*** METHODS - PUBLIC ***//
	public void SetMenuVisibilityState (bool state)
	{
		((GameGUIView)CurrentView).SetMenuVisibilityState(state);
	}

	public void SetHUDVisibilityState (bool state)
	{
		((GameGUIView)CurrentView).SetHUDVisibilityState(state);
	}

	public void SetBossHealthVisibility (bool state)
	{
		((GameGUIView)CurrentView).SetBossHealthVisibility(state);
	}

	public void StartGameStartAnimation ()
	{
		GameController.Instance.GoToGameStateWithAnimation(Enums.GameState.GAME);
		((GameGUIView)CurrentView).StartGameStartAnimation();
	}

	public void StartBossWarningAnimation ()
	{
		((GameGUIView)CurrentView).StartBossWarningAnimation();
	}

	public void StartGameOverAnimation ()
	{
		((GameGUIView)CurrentView).StartGameOverAnimation();
	}

//*** METHODS - PROTECTED ***//
	protected override void Awake ()
	{
		base.Awake();

		Instance = this;
	}

	protected override void Start ()
	{
		base.Start();
	}
	
	protected override void Update ()
	{
		base.Update();
	}

//*** METHODS - PRIVATE ***//
	
}
