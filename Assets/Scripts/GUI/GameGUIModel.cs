﻿using UnityEngine;
using System.Collections;

public class GameGUIModel : Model {

//*** ATTRIBUTES ***//
	

//*** PROPERTIES ***//
	

//*** METHODS - PUBLIC ***//


//*** METHODS - PROTECTED ***//
	protected override void Awake ()
	{
		base.Awake();

		GameActions.OnBossWaveSpawned += HandleOnBossWaveSpawnedEvent;
		GameActions.OnGameOver += HandleOnGameOverEvent;
	}

	protected override void Start ()
	{
		base.Start();
	}
	
	protected override void Update ()
	{
		base.Update();
	}

	protected void OnDestroy ()
	{
		GameActions.OnBossWaveSpawned -= HandleOnBossWaveSpawnedEvent;
		GameActions.OnGameOver -= HandleOnGameOverEvent;
	}

//*** METHODS - PRIVATE ***//
	private void HandleOnBossWaveSpawnedEvent ()
	{
		((GameGUIView)CurrentView).SetBossHealthVisibility(true);
	}

	private void HandleOnGameOverEvent (bool isSuccessful)
	{
		((GameGUIView)CurrentView).SetBossHealthVisibility(false);
	}

}
