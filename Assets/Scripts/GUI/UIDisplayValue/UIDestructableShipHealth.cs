﻿using UnityEngine;
using System.Collections;

public class UIDestructableShipHealth : UIDisplayValue {

//*** ATTRIBUTES ***//
	[Header("[ Extended references ]")]
	[SerializeField]
	private ClassValue maxClassValue;

//*** PROPERTIES ***//
	protected ClassValue MaxClassValue {
		get {return maxClassValue;}
	}

//*** METHODS - PUBLIC ***//


//*** METHODS - PROTECTED ***//
	protected override void Awake ()
	{
		base.Awake();
	}

	protected override void Start ()
	{
		base.Start();
	}
	
	protected override void Update ()
	{
		base.Update();
	}

	protected override void UpdateDisplayedValue ()
	{
		if (SourceValue == null || TargetText == null || MaxClassValue == null)
		{
			return;
		}

		int percentage = (int)((float)SourceValue.GetIntValue() / (float)MaxClassValue.GetIntValue() * 100);

		TargetText.text = string.Format(ValueFormat, percentage);
	}

//*** METHODS - PRIVATE ***//

}
