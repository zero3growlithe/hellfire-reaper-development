﻿using UnityEngine;
using UnityEngine.UI;
using System.Collections;

public class UIBossHealth : UIDestructableShipHealth {

//*** ATTRIBUTES ***//
	[SerializeField]
	private Image healthBarImage;

//*** PROPERTIES ***//
	private Image HealthBarImage {
		get {return healthBarImage;}
	}

//*** METHODS - PUBLIC ***//


//*** METHODS - PROTECTED ***//
	protected override void Awake ()
	{
		base.Awake();
	}

	protected override void Start ()
	{
		base.Start();
	}
	
	protected override void Update ()
	{
		base.Update();
	}

	protected override void UpdateDisplayedValue ()
	{
		BossShip boss = GameController.Instance.CurrentBoss;

		if (boss == null || HealthBarImage == null)
		{
			return;
		}

		// resorting to old methods due to some unknown problem
		float percentage = (float)boss.CurrentLifeCount / (float)boss.Lives;

		HealthBarImage.fillAmount = percentage;
	}

//*** METHODS - PRIVATE ***//

}
