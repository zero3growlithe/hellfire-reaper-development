﻿using UnityEngine;
using UnityEngine.UI;
using System.Collections;

public class UIDisplayValue : MonoBehaviour {

//*** ATTRIBUTES ***//
	[SerializeField]
	private ClassValue sourceValue;
	[SerializeField]
	private string valueFormat = "{0}%";

//*** PROPERTIES ***//
	protected ClassValue SourceValue {
		get {return sourceValue;}
	}
	protected string ValueFormat {
		get {return valueFormat;}
	}

	protected Text TargetText {get; set;}

//*** METHODS - PUBLIC ***//


//*** METHODS - PROTECTED ***//
	protected virtual void Awake ()
	{
		TargetText = GetComponent<Text>();
	}

	protected virtual void Start ()
	{
		
	}
	
	protected virtual void Update ()
	{
		UpdateDisplayedValue();
	}

	protected virtual void UpdateDisplayedValue ()
	{
		if (SourceValue == null || TargetText == null)
		{
			return;
		}

		TargetText.text = string.Format(ValueFormat, SourceValue.GetFloatValue());
	}

//*** METHODS - PRIVATE ***//

}
