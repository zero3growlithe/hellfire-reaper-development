﻿using UnityEngine;
using System.Collections;

public class PlayerInputController : MonoBehaviour {

//*** ATTRIBUTES ***//
	[SerializeField]
	private MonoBehaviour target;

	[Space(5)]
	[SerializeField]
	private float touchSensitivity = 0.3f;

//*** PROPERTIES ***//
	private MonoBehaviour Target {
		get {return target;}
	}
	private float TouchSensitivity {
		get {return touchSensitivity;}
	}
	private IControllable TargetInterface {
		get;
		set;
	}

	// INPUT
	private string UpKey {
		get {return GameSettingsManager.Instance.UpInputKey;}
	}
	private string DownKey {
		get {return GameSettingsManager.Instance.DownInputKey;}
	}
	private string LeftKey {
		get {return GameSettingsManager.Instance.LeftInputKey;}
	}
	private string RightKey {
		get {return GameSettingsManager.Instance.RightInputKey;}
	}

	private string ShootKey {
		get {return GameSettingsManager.Instance.ShootInputKey;}
	}

	private InputManager CustomInput {
		get {return InputManager.Instance;}
	}

//*** METHODS - PUBLIC ***//


//*** METHODS - PROTECTED ***//
	protected virtual void Awake ()
	{
		TargetInterface = Target as IControllable;
	}

	protected virtual void Start ()
	{
		
	}
	
	protected virtual void Update ()
	{
		if (GameManager.Instance.IsGamePaused == true)
		{
			return;
		}
		
		if (Input.touchSupported == false)
		{
			UpdateInput();
		}
		else
		{
			UpdateTouchInput();
		}
	}

//*** METHODS - PRIVATE ***//
	private void UpdateTouchInput ()
	{
		if (Input.touches.Length == 0)
		{
			return;
		}

		Vector2 moveDirection = Input.touches[0].deltaPosition * TouchSensitivity;

		TargetInterface.Move(moveDirection);
		TargetInterface.Shoot();
	}

	private void UpdateInput ()
	{
		if (TargetInterface == null)
		{
			Debug.Log("Target object is null or is not an IControllable.");
			return;
		}

		Vector2 moveDirection = new Vector2(
			(CustomInput.GetKey(LeftKey) ? -1 : 0) + (CustomInput.GetKey(RightKey) ? 1 : 0),
			(CustomInput.GetKey(UpKey) ? 1 : 0) + (CustomInput.GetKey(DownKey) ? -1 : 0));

		TargetInterface.Move(moveDirection);

		if (InputManager.Instance.GetKey(ShootKey) == true)		
		{
			TargetInterface.Shoot();
		}
	}

}
