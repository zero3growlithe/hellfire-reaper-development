﻿using UnityEngine;
using System.Collections;

public class GameController : MonoBehaviour {

//*** ATTRIBUTES ***//
	[Header("[ References ]")]
	[SerializeField]
	private GameObject playerShipToSpawn;
	[SerializeField]
	private ShipController playerShip;

	[Space(10)]
	[SerializeField]
	private Transform playerSpawnPoint;

	[Header("[ Settings ]")]
	[SerializeField]
	private Enums.GameState currentGameState;
	[SerializeField]
	private string bossWaveName = "Boss";

//*** PROPERTIES ***//
	public static GameController Instance {get; private set;}

	// REFERENCES
	public GameObject PlayerShipToSpawn {
		get {return playerShipToSpawn;}
	}
	public Transform PlayerSpawnPoint {
		get {return playerSpawnPoint;}
	}
	
	// SETTINGS
	public Enums.GameState CurrentGameState {
		get {return currentGameState;}
		private set {currentGameState = value;}
	}
	private string BossWaveName {
		get {return bossWaveName;}
	}

	private Enums.GameState TargetGameState {get; set;}

	// VARIABLES
	public BossShip CurrentBoss {get; private set;}
	public ShipController PlayerShip {
		get {return playerShip;}
		private set {playerShip = value;}
	}
	public Transform PlayerShipTransform {
		get {return PlayerShip != null ? PlayerShip.transform : null;}
	}

//*** METHODS - PUBLIC ***//
	public void SpawnStartupLevel ()
	{
		LevelManager.Instance.SpawnLevelByName(Enums.LevelName.LEVEL_0);
		GameActions.Instance.NotifyOnLevelLoaded();
	}

	public void StartGame ()
	{
		CurrentGameState = TargetGameState;
		LevelWavesController.Instance.SetRunningState(true);

		SpawnPlayerShip();
		GameActions.Instance.NotifyOnLevelStart();
	}

	public void QuitGame ()
	{
		ResetGame();
	}

	public void GoToGameStateWithAnimation (Enums.GameState state)
	{
		TargetGameState = state;
		CurrentGameState = Enums.GameState.ANIMATION;
	}

	public void TogglePause ()
	{
		if (CurrentGameState != Enums.GameState.GAME && CurrentGameState != Enums.GameState.PAUSE)
		{
			return;
		}

		GameManager.Instance.PauseGame(!GameManager.Instance.IsGamePaused);

		GameGUIController.Instance.SetHUDVisibilityState(!GameManager.Instance.IsGamePaused);
		GameGUIController.Instance.SetMenuVisibilityState(GameManager.Instance.IsGamePaused);

		if (GameManager.Instance.IsGamePaused == true)
		{
			MenuController.Instance.GoToScreen(typeof(PauseMenuScreen));
		}
	}

	public void QuitToScoreboard ()
	{
		ResetGame();

		MenuController.Instance.ClearScreenCallStack();
		MenuController.Instance.GoToNextScreen(typeof(MainMenuScreen));
		MenuController.Instance.GoToNextScreen(typeof(ScoresScreen));
	}

	public void SetCurrentBoss (BossShip boss)
	{
		CurrentBoss = boss;
	}

//*** METHODS - PROTECTED ***//
	protected virtual void Awake ()
	{
		DontDestroyOnLoad(gameObject);

		if (Instance == null)
		{
			Instance = this;
		}
		else if (Instance != this)
		{
			Destroy(gameObject);
		}
	}

	protected virtual void Start ()
	{
		AttachToEvents();
		SpawnPlayerShip();
		SetStartGameProperties();
	}
	
	protected virtual void Update ()
	{
		HandleInput();
	}

	protected void OnDestroy ()
	{
		DetachFromEvents();
	}

//*** METHODS - PRIVATE ***//
	// INPUT
	private void HandleInput ()
	{
		if (InputManager.Instance.GetKeyDown(GameSettingsManager.Instance.EscapeInputKey))
		{
			TryToPauseGame();
		}
	}

	private void TryToPauseGame ()
	{
		TogglePause();
	}

	// PLAYER SHIP HANDLING
	private void SpawnPlayerShip ()
	{
		if (CurrentGameState != Enums.GameState.GAME)
		{
			return;
		}

		if (PlayerShip != null)
		{
			PlayerShip.Initialize();
			TryToRespawnPlayer();

			return;
		}

		GameObject spawnedShip = (GameObject)Instantiate(PlayerShipToSpawn, PlayerSpawnPoint.position, PlayerSpawnPoint.rotation);
		PlayerShip = spawnedShip.GetComponent<ShipController>();

		PlayerShip.Respawn(false);
	}

	private void TryToRespawnPlayer ()
	{
		PlayerShip.Respawn();
		PlayerShipTransform.position = PlayerSpawnPoint.position;
	}
	
	// GAME CONTROLS
	private void ResetGame ()
	{
		GameManager.Instance.PauseGame(false);
		LevelManager.Instance.ClearSpawnedLevel();

		CurrentGameState = Enums.GameState.MAIN_MENU;

		SetStartGameProperties();
		CleanUp();

		MenuController.Instance.ClearScreenCallStack();
		MenuController.Instance.GoToScreen(typeof(TitleScreen));
	}

	private void SetStartGameProperties ()
	{
		ScreenWidthController.Instance.CloseScreen(CurrentGameState == Enums.GameState.GAME);

		if (CurrentGameState == Enums.GameState.GAME)
		{
			SpawnPlayerShip();
			SpawnStartupLevel();

			LevelWavesController.Instance.SetRunningState(true);
		}
		else if (PlayerShip != null)
		{
			PlayerShip.gameObject.SetActive(false);
		}

		GameGUIController.Instance.SetHUDVisibilityState(CurrentGameState == Enums.GameState.GAME);
		GameGUIController.Instance.SetMenuVisibilityState(CurrentGameState == Enums.GameState.MAIN_MENU);
	}

	private void CleanUp ()
	{
		SpawnedJunkManager.Instance.DestroyJunk();
		BulletsManager.Instance.DeactivateAllBullets();
	}

	// EVENTS HANDLERS
	private void AttachToEvents ()
	{
		GameActions.OnGamePauseChanged += HandleOnGamePauseChangedEvent;
		GameActions.OnGameRestart += HandleOnGameRestartEvent;
		GameActions.OnQuitToMenu += HandleOnQuitToMenuEvent;
		GameActions.OnLevelLoaded += HandleOnLevelLoadedEvent;
		GameActions.OnGameOver += HandleOnGameOverEvent;
		GameActions.OnShipDestroyed += HandleOnShipDestroyedEvent;
		GameActions.OnActivateNextEnemyWave += HandleOnActivateNextEnemyWaveEvent;
	}

	private void DetachFromEvents ()
	{
		GameActions.OnGamePauseChanged -= HandleOnGamePauseChangedEvent;
		GameActions.OnGameRestart -= HandleOnGameRestartEvent;
		GameActions.OnQuitToMenu -= HandleOnQuitToMenuEvent;
		GameActions.OnLevelLoaded -= HandleOnLevelLoadedEvent;
		GameActions.OnGameOver -= HandleOnGameOverEvent;
		GameActions.OnShipDestroyed -= HandleOnShipDestroyedEvent;
		GameActions.OnActivateNextEnemyWave -= HandleOnActivateNextEnemyWaveEvent;
	}

	private void HandleOnGamePauseChangedEvent (bool state)
	{
		if (state == true)
		{
			CurrentGameState = Enums.GameState.PAUSE;
		}
		else
		{
			CurrentGameState = Enums.GameState.GAME;
		}
	}

	private void HandleOnGameRestartEvent ()
	{
		CurrentGameState = Enums.GameState.GAME;
	}

	private void HandleOnQuitToMenuEvent ()
	{
		ScreenWidthController.Instance.CloseScreen(false);

		CurrentGameState = Enums.GameState.MAIN_MENU;
	}

	private void HandleOnLevelLoadedEvent ()
	{

	}

	private void HandleOnGameOverEvent (bool isSuccessful)
	{
		if (isSuccessful == true)
		{
			QuitToScoreboard();
		}
		else
		{
			GameGUIController.Instance.StartGameOverAnimation();
		}
	}

	private void HandleOnShipDestroyedEvent (DestructableShip ship)
	{
		if (ship.gameObject == PlayerShip.gameObject)
		{
			CancelInvoke("TryToRespawnPlayer");
			Invoke("TryToRespawnPlayer", GameSettingsManager.Instance.PlayerRespawnDelay);
		}
	}

	private void HandleOnActivateNextEnemyWaveEvent (EnemyWave currentWave)
	{
		if (currentWave.Name != BossWaveName)
		{
			return;
		}

		GameGUIController.Instance.StartBossWarningAnimation();
		GameActions.Instance.NotifyOnBossWaveSpawned();
	}
}