﻿using UnityEngine;
using UnityEngine.UI;
using System.Collections;

[ExecuteInEditMode()]
public class ScreenCoverController : MonoBehaviour {

//*** ATTRIBUTES ***//
	[SerializeField]
	private RectTransform leftCover;
	[SerializeField]
	private RectTransform rightCover;

	[Space(10)]
	[SerializeField]
	private Transform rootCanvas;
	[SerializeField]
	private Camera sceneCamera;

//*** PROPERTIES ***//
	private RectTransform LeftCover {
		get {return leftCover;}
	}
	private RectTransform RightCover {
		get {return rightCover;}
	}

	private Transform RootCanvas {
		get {return rootCanvas;}
	}
	private Camera SceneCamera {
		get {return sceneCamera;}
	}

	private RectTransform RootCanvasRect {get; set;}
	private float LastScreenWidth {get; set;}

//*** METHODS - PUBLIC ***//


//*** METHODS - PROTECTED ***//
	protected virtual void Awake ()
	{

	}

	protected virtual void Start ()
	{
		ScreenWidthController.OnScreenWidthChanged += UpdateScreenCoverSize;

		UpdateScreenCoverSize();
	}
	
	protected virtual void Update ()
	{
		if (Application.isPlaying == false || ScreenWidthController.Instance.ScreenWidth != LastScreenWidth)
		{
			if (RootCanvasRect == null)
			{
				RootCanvasRect = RootCanvas.GetComponent<RectTransform>();
			}

			UpdateScreenCoverSize();
		}
		else
		{
			LastScreenWidth = ScreenWidthController.Instance.ScreenWidth;
		}
	}

	protected void OnDestroy ()
	{
		ScreenWidthController.OnScreenWidthChanged -= UpdateScreenCoverSize;
	}

//*** METHODS - PRIVATE ***//
	private void UpdateScreenCoverSize ()
	{
		if (CheckIfReferencesCorrect() == false)
		{
			return;
		}

		float visibleScreenWidth = RootCanvasRect.rect.width * SceneCamera.rect.width;
		float coverWidth = (RootCanvasRect.rect.width - visibleScreenWidth) / 2f;

		Vector2 leftCoverSize = LeftCover.sizeDelta;
		Vector2 rightCoverSize = RightCover.sizeDelta;

		leftCoverSize.x = coverWidth;
		rightCoverSize.x = coverWidth;

		LeftCover.sizeDelta = leftCoverSize;
		RightCover.sizeDelta = rightCoverSize;
	}

	private bool CheckIfReferencesCorrect ()
	{
		if (LeftCover == null || RightCover == null ||
			RootCanvas == null || RootCanvasRect == null || SceneCamera == null)
		{
			return false;
		}

		return true;
	}

}
