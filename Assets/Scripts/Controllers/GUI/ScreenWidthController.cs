﻿using UnityEngine;
using System;
using System.Collections;

[ExecuteInEditMode()]
[RequireComponent(typeof(Camera))]
public class ScreenWidthController : MonoBehaviour {

//*** ATTRIBUTES ***//
	[Header("[ References ]")]
	[SerializeField]
	private Camera targetCamera;

	[Header("[ Settings ]")]
	[SerializeField]
	[Range(0.00f, 1.00f)]
	private float screenWidth = 1.0f;
	[SerializeField]
	private float closeAnimationSpeed = 20.0f;

	// ACTIONS
	public static Action OnScreenWidthChanged = delegate {};

//*** PROPERTIES ***//
	public static ScreenWidthController Instance {get; private set;}

	// REFERENCES
	private Camera TargetCamera {
		get {return targetCamera;}
	}
	
	// SETTINGS
	public float ScreenWidth {
		get {return screenWidth;}
		private set {screenWidth = value;}
	}
	private float CloseAnimationSpeed {
		get {return closeAnimationSpeed;}
	}

	// VARIABLES
	public float UsablePixelScreenWidth {
		get {
			return Screen.width * ScreenWidth;
		}
	}
	public Rect UsableScreenRect {get; private set;}
	private float DefaultWidth {
		get {return GameSettingsManager.Instance.DefaultScreenWidth;}
	}

//*** METHODS - PUBLIC ***//
	// CONTROLS
	public void CloseScreen (bool state = true)
	{
		StopAllCoroutines();
		StartCoroutine(MoveScreenWidthToTarget(state == true ? DefaultWidth : 1.0f));
	}

	public void SetScreenWidth (float value)
	{
		ScreenWidth = Mathf.Clamp01(value);

		UpdateScreenWidth();
		OnScreenWidthChanged();
	}

//*** METHODS - PROTECTED ***//
	protected virtual void Awake ()
	{
		Instance = this;
		
		UsableScreenRect = new Rect(0, 0, Screen.width, Screen.height);
	}

	protected virtual void Start ()
	{
		UpdateScreenWidth();
	}
	
	protected virtual void Update ()
	{
		if (Application.isPlaying == false)
		{
			UpdateScreenWidth();
		}
	}

//*** METHODS - PRIVATE ***//
	private IEnumerator MoveScreenWidthToTarget (float targetWidth)
	{
		while (ScreenWidth != targetWidth)
		{
			SetScreenWidth(Mathf.MoveTowards(ScreenWidth, targetWidth, CloseAnimationSpeed * Time.unscaledDeltaTime));

			yield return null;
		}

		yield break;
	}

	private void UpdateScreenWidth ()
	{
		if (TargetCamera == null)
		{
			return;
		}

		// get values
		Rect screenRect = TargetCamera.rect;

		Vector2 screenSize = screenRect.size;
		Vector2 screenCenter = screenRect.center;

		// apparently this step is not necessary because f* you?
		// float screenOffset = (1.0f - ScreenWidth) * 0.5f;

		// update values
		screenSize.x = ScreenWidth;

		// set values
		screenRect.size = screenSize; // this f*** thing doesn't make any f*** sense
		// when unity makes something "automatically" it f***** sucks

		screenRect.center = screenCenter;

		// apply changes
		TargetCamera.rect = screenRect;

		// update usable screen rect
		UsableScreenRect = new Rect(screenCenter.x * Screen.width, 0, UsablePixelScreenWidth, Screen.height);
	}

}
