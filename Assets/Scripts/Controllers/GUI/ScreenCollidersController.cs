﻿using UnityEngine;
using UnityEngine.UI;
using System.Collections;

[ExecuteInEditMode()]
public class ScreenCollidersController : MonoBehaviour {

//*** ATTRIBUTES ***//
	[SerializeField]
	private Collider2D leftCollider;
	[SerializeField]
	private Collider2D rightCollider;
	[SerializeField]
	private Collider2D topCollider;
	[SerializeField]
	private Collider2D bottomCollider;

	[Space(10)]
	[SerializeField]
	private Camera sceneCamera;

//*** PROPERTIES ***//
	public static ScreenCollidersController Instance {get; private set;}

	// REFERENCES
	private Collider2D LeftCollider {
		get {return leftCollider;}
	}
	private Collider2D RightCollider {
		get {return rightCollider;}
	}
	private Collider2D TopCollider {
		get {return topCollider;}
	}
	private Collider2D BottomCollider {
		get {return bottomCollider;}
	}

	// VARIABLES
	public float MaxVisibleOffsetWidth {get; private set;}
	public float MaxVisibleOffsetHeight {get; private set;}

	private Camera SceneCamera {
		get {return sceneCamera;}
	}
	private float LastScreenWidth {get; set;}

//*** METHODS - PUBLIC ***//


//*** METHODS - PROTECTED ***//
	protected virtual void Awake ()
	{
		Instance = this;
	}

	protected virtual void Start ()
	{
		UpdateScreenCollidersSize();
	}
	
	protected virtual void Update ()
	{
		if (Application.isPlaying == false || ScreenWidthController.Instance.ScreenWidth != LastScreenWidth)
		{
			UpdateScreenCollidersSize();
		}
		else
		{
			LastScreenWidth = ScreenWidthController.Instance.ScreenWidth;
		}
	}

//*** METHODS - PRIVATE ***//
	private void UpdateScreenCollidersSize ()
	{
		if (CheckIfReferencesCorrect() == false)
		{
			return;
		}

		float screenWidth = SceneCamera.rect.width;
		float screenHeight = SceneCamera.rect.height;
		float aspectRatio = (float)Screen.width / (float)Screen.height;
		float cameraSize = SceneCamera.orthographicSize;

		MaxVisibleOffsetWidth = aspectRatio * screenWidth * cameraSize;
		MaxVisibleOffsetHeight = screenHeight * cameraSize;

		LeftCollider.transform.position = new Vector3(-MaxVisibleOffsetWidth, 0, 0);
		RightCollider.transform.position = new Vector3(MaxVisibleOffsetWidth, 0, 0);
		TopCollider.transform.position = new Vector3(0, MaxVisibleOffsetHeight, 0);
		BottomCollider.transform.position = new Vector3(0, -MaxVisibleOffsetHeight, 0);
	}

	private bool CheckIfReferencesCorrect ()
	{
		if (LeftCollider == null || RightCollider == null ||
			TopCollider == null || BottomCollider == null || SceneCamera == null)
		{
			return false;
		}

		return true;
	}

}
