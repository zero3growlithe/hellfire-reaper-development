﻿using UnityEngine;
using System.Collections;

public class MusicController : MonoBehaviour {

//*** ATTRIBUTES ***//
	[Header("[ References ]")]
	[SerializeField]
	private MusicReference[] musicCollection;

	[Header("[ Settings ]")]
	[SerializeField]
	private float musicCrossfadeSpeed = 5f;

//*** PROPERTIES ***//
	public static MusicController Instance {get; private set;}

	// REFERENCES
	private MusicReference[] MusicCollection {
		get {return musicCollection;}
	}

	// SETTINGS
	private float MusicCrossfadeSpeed {
		get {return musicCrossfadeSpeed;}
	}

	// VARIABLES
	private AudioSource FirstAudioSource {get; set;}
	private AudioSource SecondAudioSource {get; set;}

//*** METHODS - PUBLIC ***//


//*** METHODS - PROTECTED ***//
	protected virtual void Awake ()
	{
		Instance = this;

		Initialize();
	}

	protected virtual void Start ()
	{
		
	}
	
	protected virtual void Update ()
	{
		
	}

	protected void Initialize ()
	{
		FirstAudioSource = gameObject.AddComponent<AudioSource>();
		SecondAudioSource = gameObject.AddComponent<AudioSource>();

		SetUpAudioSource(FirstAudioSource);
		SetUpAudioSource(SecondAudioSource);
	}

//*** METHODS - PRIVATE ***//
	private void SetUpAudioSource (AudioSource target)
	{
		target.playOnAwake = false;
		target.spatialBlend = 0;
	}

//*** ENUMS ***//
	[System.Serializable]
	public class MusicReference {
		[SerializeField]
		private string name;
		[SerializeField]
		private AudioClip audioFile;

		public string Name {
			get {return name;}
		}
		public AudioClip AudioFile {
			get {return audioFile;}
		}
	}

}
