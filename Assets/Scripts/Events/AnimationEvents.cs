﻿using UnityEngine;
using System.Collections;

public class AnimationEvents : MonoBehaviour {

//*** ATTRIBUTES ***//
	[SerializeField]
	private string nextFrameTriggerName = "NextAnimation";

//*** PROPERTIES ***//
	private string NextFrameTriggerName {
		get {return nextFrameTriggerName;}
	}	

//*** METHODS - PUBLIC ***//
	public void StartGame ()
	{
		GameController.Instance.StartGame();
	}

	public void SpawnStartupLevel ()
	{
		GameController.Instance.SpawnStartupLevel();
	}

	public void HideMenu ()
	{
		GameGUIController.Instance.SetMenuVisibilityState(false);
	}

	public void ShowMenu ()
	{
		GameGUIController.Instance.SetMenuVisibilityState(true);
	}

	public void NotifyOnGameWon ()
	{
		GameActions.Instance.NotifyOnGameOver(true);
	}

	public void ShowScoreboard ()
	{
		GameController.Instance.QuitToScoreboard();		
	}

	public void HideHUD ()
	{
		GameGUIController.Instance.SetHUDVisibilityState(false);
	}

	public void ShowHUD ()
	{
		GameGUIController.Instance.SetHUDVisibilityState(true);
	}

	public void CloseScreen ()
	{
		ScreenWidthController.Instance.CloseScreen(true);
	}

	public void OpenScreen ()
	{
		ScreenWidthController.Instance.CloseScreen(false);
	}

	public void DisableObject ()
	{
		gameObject.SetActive(false);
	}

	public void DestroyObject ()
	{
		Destroy(gameObject);
	}

	public void TriggerNextAnimation ()
	{
		Animator targetAnimator = GetComponent<Animator>();

		targetAnimator.SetTrigger(NextFrameTriggerName);
	}

//*** METHODS - PROTECTED ***//
	

//*** METHODS - PRIVATE ***//

}
