﻿using UnityEngine;
using System;
using System.Collections;
using System.Collections.Generic;

public class GameActions : MonoBehaviour {

//*** ATTRIBUTES ***//
	// STATUS
	public static Action<bool> OnGamePauseChanged = delegate{};
	public static Action OnGameRestart = delegate{};
	public static Action OnQuitToMenu = delegate{};

	public static Action OnLevelLoaded = delegate{};
	public static Action OnLevelStart = delegate{};

	public static Action<bool> OnGameOver = delegate{};

	// GAMEPLAY
	public static Action<BulletHit> OnBulletHit = delegate{};
	public static Action<DestructableShip> OnShipDestroyed = delegate{};

	public static Action<EnemyWave> OnActivateNextEnemyWave = delegate{};
	public static Action OnBossWaveSpawned = delegate{};

//*** PROPERTIES ***//
	public static GameActions Instance {get; private set;}

//*** METHODS - PUBLIC ***//
	// STATUS
	public void NotifyOnGamePauseChanged (bool state)
	{
		OnGamePauseChanged(state);
	}

	public void NotifyOnGameRestart ()
	{
		OnGameRestart();
	}

	public void NotifyOnQuitToMenu ()
	{
		OnQuitToMenu();
	}

	public void NotifyOnLevelLoaded ()
	{
		OnLevelLoaded();
	}

	public void NotifyOnLevelStart ()
	{
		OnLevelStart();
	}

	public void NotifyOnGameOver (bool isSuccessful)
	{
		OnGameOver(isSuccessful);
	}

	// GAMEPLAY
	public void NotifyOnBulletHit (BulletHit hit)
	{
		OnBulletHit(hit);
	}

	public void NotifyOnShipDestroyed (DestructableShip ship)
	{
		OnShipDestroyed(ship);
	}

	public void NotifyOnActivateNextEnemyWave (EnemyWave wave)
	{
		OnActivateNextEnemyWave(wave);
	}

	public void NotifyOnBossWaveSpawned ()
	{
		OnBossWaveSpawned();
	}

//*** METHODS - PROTECTED ***//
	protected virtual void Awake ()
	{
		Instance = this;
	}

	protected virtual void Start ()
	{
		
	}
	
	protected virtual void Update ()
	{
		
	}

//*** METHODS - PRIVATE ***//

}
