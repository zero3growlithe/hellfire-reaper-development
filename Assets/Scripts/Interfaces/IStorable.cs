﻿using UnityEngine;
using System.Collections;

public interface IStorable {
	void Save ();
	void Load ();
}
