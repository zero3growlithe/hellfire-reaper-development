﻿using UnityEngine;
using System.Collections;

public interface IControllable {

	void Move (Vector2 direction);
	void Shoot ();

}
