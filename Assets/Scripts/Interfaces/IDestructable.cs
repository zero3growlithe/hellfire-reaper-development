﻿using UnityEngine;
using System.Collections;

public interface IDestructable {

//*** ATTRIBUTES ***//
	

//*** PROPERTIES ***//
	

//*** METHODS - PUBLIC ***//


//*** METHODS - PROTECTED ***//
	void Destroy ();
	void Revive ();

//*** METHODS - PRIVATE ***//

}
