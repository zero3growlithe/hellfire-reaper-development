﻿using UnityEngine;
using System.Collections;

public interface IHideable {

//*** ATTRIBUTES ***//
	

//*** PROPERTIES ***//
	

//*** METHODS - PUBLIC ***//
	void Activate (bool state = true);
	void Deactivate ();

//*** METHODS - PROTECTED ***//
	

//*** METHODS - PRIVATE ***//

}
