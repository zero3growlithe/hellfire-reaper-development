﻿using UnityEngine;
using System.Collections;

public interface IObstacle {

//*** ATTRIBUTES ***//
	

//*** PROPERTIES ***//
	

//*** METHODS - PUBLIC ***//
	void NotifyOnObstacleHit ();

//*** METHODS - PROTECTED ***//
	

//*** METHODS - PRIVATE ***//

}
